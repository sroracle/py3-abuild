README for py3-abuild
=====================

:Authors:
  * **Max Rees**, primary developer
  * **Samuel Holland**, contributor
:Status:
  Alpha
:Copyright:
  © 2018 Max Rees. GPL-2.0 and MIT open source licenses.

Synopsis
--------

This is a **work-in-progress** rewrite of the
`abuild <https://git.alpinelinux.org/cgit/abuild/>`_ build system for Alpine
Package Keeper-based Linux operating system distributions. It intends to also
provide a library from which other applications can be built.

Dependencies
------------

* Python 3.6+

Installation
------------

.. code-block:: console

   make
   make check
   make install DESTDIR=path/to/install/location

Usage
-----
The package comes with a command line interface currently called ``pbuild``.
It is intended to be a mostly-compatible replacement for ``abuild``. See
``pbuild --help`` for more information.

See the `TODO <TODO.rst>`_ file for more information.
