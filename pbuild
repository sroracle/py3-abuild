#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-2.0-only
# Copyright (c) 2018 Max Rees
# See LICENSE for more information.
# pylint: disable=invalid-name,redefined-builtin
from sys import argv, exit, stderr
from os import environ, geteuid
from argparse import ArgumentParser, RawDescriptionHelpFormatter, SUPPRESS

import abuild.config as conf
import abuild.common as com
import abuild.exception as exc
from abuild import Abuild

PRINT_TB = False

def get_phase_help():
    helptext = ""
    for phasename in conf.AVAILABLE_PHASES:
        if phasename in conf.SHELL_PHASES or phasename.startswith("_"):
            continue
        phasefunc = getattr(Abuild, phasename)
        desc = phasefunc.__doc__
        if not desc:
            continue
        desc = desc.splitlines()[3].strip()
        helptext += f"{phasename:16}{desc}\n"

    helptext += f"{'build':16}Compile the package\n"
    helptext += f"{'package':16}Install the main package to $pkgdir\n"
    helptext += f"{'<split func>':16}Install the subpackage to $subpkgdir\n"
    return helptext

try:
    opts = ArgumentParser(
        formatter_class=RawDescriptionHelpFormatter,
        usage="pbuild [options...] [PHASE [PHASE...]]",
        add_help=False,
        epilog=f"""
To activate cross compilation specify in environment:
  {'CHOST':16}Arch or hostspec of machine to generate packages for
  {'CTARGET':16}Arch or hostspec of machine to generate compiler for
""".strip())

    general = opts.add_argument_group("General options")
    general.add_argument(
        "-h", "--help", action="help",
        help="Display this help message")
    general.add_argument(
        "-F", dest="force_root", action="store_true",
        help="Force run as root")
    general.add_argument(
        "-A", dest="print_carch", action="store_true",
        help="Print CARCH and exit")
    general.add_argument(
        "-D", dest="description",
        help="Set APKINDEX description (default: $repo $(git describe))")
    general.add_argument(
        "-P", dest="REPODEST",
        help="Set REPODEST as the repository location for created packages")
    general.add_argument(
        "-s", dest="SRCDEST",
        help="Set source package destination directory")
    general.add_argument(
        "-f", dest="force", action="store_true",
        help="Force specified PHASE(s)")
    # TODO implement
    general.add_argument(
        "-i", dest="install_after", action="store_true",
        help="Install PKG after successful build")
    general.add_argument(
        "-k", dest="keep_pkg", action="store_true",
        help="Keep built packages, even if APKBUILD or sources are newer")
    general.add_argument(
        "-K", dest="keep_tmp", action="store_true",
        help="Keep buildtime temp dirs and files (srcdir/pkgdir/deps)")
    general.add_argument(
        "--pedantic", action="store_true",
        help="Enable strict APKBUILD parsing")
    general.add_argument(
        "--mk-untrusted-index", action="store_true",
        help="Allow creation of APKINDEX with untrusted signature")

    rootbld = opts.add_argument_group("Buildroot options")
    rootbld.add_argument(
        "--root-nonce", metavar="NONCE",
        help="Set buildroot path to /var/tmp/abuild.NONCE")
    rootbld.add_argument(
        "--upgrade-root", action="store_true",
        help="Upgrade buildroot before continuing")

    output = opts.add_argument_group("Logging options")
    output.add_argument(
        "-v", dest="verbose", action="store_true",
        help="Verbose (show every command as it is run, and more)")
    output.add_argument(
        "-q", dest="quiet", default=False, action="store_true",
        help="Quiet (show only warnings, errors, and shell output)")
    output.add_argument(
        "-c", dest="colors", default=SUPPRESS, action="store_true",
        help="Enable colored output (overrides USE_COLORS)")
    output.add_argument(
        "-m", dest="colors", default=SUPPRESS, action="store_false",
        help="Disable colored output (overrides USE_COLORS)")
    output.add_argument(
        "-t", dest="time", default=False, action="store_true",
        help="Show time in logging messages")
    output.add_argument(
        "-l", dest="logfile", default=None,
        help="Log all output to LOGFILE")

    deps = opts.add_argument_group("Dependency options")
    deps.add_argument(
        "-d", dest="deps", action="store_const",
        const=com.DepOpts.IGNORE,
        help="Disable dependency checking")
    deps.add_argument(
        "-r", dest="deps", action="store_const",
        const=com.DepOpts.INSTALL,
        help="Install missing dependencies using default repos")
    deps.add_argument(
        "-R", dest="deps", action="store_const",
        const=com.DepOpts.BUILD,
        help="Recursively build and install missing dependencies")
    deps.add_argument(
        "-u", dest="deps", action="store_const",
        const=com.DepOpts.UPGRADE,
        help="Recursively build and upgrade all dependencies")
    deps.add_argument(
        "--no-dag", dest="dag", action="store_const",
        const=com.DAGOpts.NO,
        help="Do not construct a DAG to optimize build order")
    deps.add_argument(
        "--dag-only", dest="dag", action="store_const",
        const=com.DAGOpts.ONLY, help="Print DAG build order and exit")
    deps.add_argument(
        "--dag-only-dot", dest="dag", action="store_const",
        const=com.DAGOpts.DOT,
        help="Print DAG build order in DOT form and exit")

    phases = opts.add_argument_group("Available phases", get_phase_help())
    phases.add_argument(
        "phases", metavar="PHASE", nargs="*", default=["all"],
        help=SUPPRESS)
    opts = opts.parse_args()

    com.conf_env()

    if opts.print_carch:
        print(environ["CARCH"])
        exit(0)

    if opts.verbose:
        level = "DEBUG"
        PRINT_TB = True
    else:
        if opts.quiet:
            level = "WARNING"
        else:
            level = "INFO"

    # Giving the -c or -m command line option overrides everything. Otherwise,
    # if we're logging to a file we ignore USE_COLORS and default to no colors.
    # If we're logging to stdout/stderr, then use the value of USE_COLORS.
    if hasattr(opts, "colors"):
        environ["USE_COLORS"] = "y" if opts.colors else ""
    elif opts.logfile:
        opts.colors = False
        environ["USE_COLORS"] = ""
    else:
        opts.colors = not com.unset_or_null(environ, "USE_COLORS")

    if opts.logfile:
        try:
            # Open in append mode so that child pbuild processes (whether in
            # fakeroot or e.g. a default_prepare) don't truncate the log
            logfile = open(opts.logfile, mode="a", encoding="utf-8")
        except Exception as e:  # pylint: disable=broad-except
            print(f"Could not open {opts.logfile}: {e}", file=stderr)
            exit(1)
    else:
        logfile = None

    # If logfile == None, log abuild messages to stderr. Otherwise to logfile.
    logger = com.init_logger(
        "abuild", output=logfile or stderr, level=level, colors=opts.colors,
        time=opts.time)
    # If logfile == None, log child process output to stdout/stderr as
    # appropriate. Otherwise to logfile.
    logger.logfile = logfile

    if opts.REPODEST:
        environ["REPODEST"] = opts.REPODEST

    if opts.SRCDEST:
        environ["SRCDEST"] = opts.SRCDEST

    if opts.description:
        environ["DESCRIPTION"] = opts.description

    if opts.dag is None:
        opts.dag = com.DAGOpts.YES
    elif opts.dag & com.DAGOpts.ONLY:
        opts.phases = ["deps"]

    if not opts.deps:
        opts.deps = com.DepOpts.NONE
        # If "deps" is given as a phase and none of (-d, -r, -R, -u,
        # --dag-only, --dag-only-dot) are given, then the user probably just
        # wants to install the dependencies from the default repos
        if "deps" in opts.phases:
            if opts.dag == com.DAGOpts.NO or opts.dag == com.DAGOpts.YES:
                opts.deps = com.DepOpts.INSTALL

    # Regardless of whether "deps" was given, it makes no sense to keep
    # --dag if none of (-d, -R, or -u) were given
    if opts.dag == com.DAGOpts.YES:
        if not opts.deps or opts.deps == com.DepOpts.INSTALL:
            opts.dag = com.DAGOpts.NO

    if __name__ != "__main__":
        com.fatal_error("Do not import pbuild")

    if geteuid() == 0 and com.unset_or_null(environ, "FAKEROOTKEY"):
        if not opts.force_root:
            com.fatal_error("Do not run as root")
        else:
            environ["FAKEROOT"] = ""

    clean_args = ""
    for arg in argv[1:]:
        if arg not in opts.phases:
            clean_args += " " + arg
    environ["ABUILD_OPTS"] = clean_args

    a = Abuild(
        deps=opts.deps,
        dag=opts.dag,
        force=opts.force,
        colors=opts.colors,
        loglevel=level,
        keep_pkg=opts.keep_pkg,
        keep_tmp=opts.keep_tmp,
        pedantic=opts.pedantic,
        mk_untrusted_index=opts.mk_untrusted_index,
        root_nonce=opts.root_nonce or "",
        upgrade_root=opts.upgrade_root,
    )

    if (opts.deps & com.DepOpts.BUILD
            and not com.unset_or_null(environ, "CBUILDROOT")):
        com.fatal_error(
            "Recursive builds (-R) are not supported when cross compiling.")

    for phase in opts.phases:
        a.do_phase(phase)

    if (logfile and com.unset_or_null(environ, "FAKEROOTKEY")
            and not any(phase.startswith("default_") for phase in opts.phases)):
        print("Build cleared!")

except exc.abuildException:
    if PRINT_TB:
        raise
    else:
        try:
            if com.unset_or_null(a.env, "FAKEROOTKEY"):
                a.cleanup(error=True)
        except Exception:  # pylint: disable=broad-except
            pass

        exit(1)
