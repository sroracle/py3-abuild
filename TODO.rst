Big picture to-do list
======================

Things to do before first release:

* The rest of the command line options
* Documentation
* Designation of public and internal APIs

Future releases:

* Rewrite default subpackage functions in Python
* Rewrite environment configuration functions to merge ``dict``
* Crosscompile support
* Write ``APKBUILD`` class as a full parser instead of shelling out
