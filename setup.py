#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-2.0-only
# Copyright (c) 2018 Max Rees
# See LICENSE for more information.
from setuptools import setup

setup(
    name="py3-abuild",
    version="0.1rc1",
    description="Python-based APK package builder",
    author="Max Rees",
    author_email="maxcrees@me.com",
    url="https://code.foxkit.us/sroracle/py3-abuild",
    license="GPL-2.0-only AND MIT",
    packages=["abuild"],
    scripts=["pbuild"],
    package_data={"abuild": ["helpers/*.sh", "helpers/printenv"]},
    python_requires=">=3.6",
    zip_safe=False,
)
