# SPDX-License-Identifier: GPL-2.0-only
# Copyright (c) 2018 Max Rees
# See LICENSE for more information.
"""
Module abuild
====================

.. module:: abuild
   :synopsis: Build packages for systems that use the Alpine Package Keeper.

This module contains the :class:`.Abuild` class, which is used to build
packages according to the specifications in :class:`.APKBUILD` files.
"""
import os       # chdir
import tempfile # NamedTemporaryFile
from pathlib import Path

import abuild.config as conf
import abuild.common as com
import abuild.child as child
import abuild.exception as exc
import abuild.file
import abuild.deps
import abuild.repo
import abuild.setup
import abuild.build
import abuild.package
import abuild.rootbld

class Subpackage:
    # pylint: disable=too-many-arguments
    def __init__(self, name, split, arch, parent, env):
        self.pkgname = name
        self.split = split
        self.arch = arch

        self.pkgver = parent.pkgver
        self.pkgrel = parent.pkgrel
        self.origin = parent.pkgname
        self.commit = parent.commit
        self.maintainer = parent.maintainer

        self.startdir = env["startdir"]
        self.pkgdir = env["pkgbasedir"] / name
        self.ctrldir = env["pkgbasedir"] / (".control." + name)

        self.fakeroot = env["FAKEROOT"]
        self.packager = env["PACKAGER"]

        self.PKGINFO = abuild.file.PKGINFO(self.ctrldir / ".PKGINFO")

        self.msgname = f"abuild.{self.origin}.{self.pkgname}"
        if not com.unset_or_null(env, "ABUILD_BUILDROOT"):
            self.msgname += "@"
        if not com.unset_or_null(env, "FAKEROOTKEY"):
            self.msgname += "*"

        self.metafiles = []
        self.rpaths = set()
        self.provides_so = {}
        self.needs_so = []
        self.symlinks = {}
        self.symlinks_needs = []
        self.provides_pc = {}
        self.needs_pc = []
        self.cmds = {}

        self.capture = {}
        self.strip_cmd = self.get_strip_cmd(env)

    def __getattr__(self, name):
        # UNDOC
        return self.capture[name]

    def get_strip_cmd(self, env):
        if self.arch == env["CBUILD_ARCH"]:
            cmd = "strip"
        elif self.arch == env["CARCH"]:
            cmd = env["CHOST"] + "-strip"
        elif self.arch == env["CTARGET_ARCH"]:
            cmd = env["CTARGET"] + "-strip"
        else:
            cmd = "strip"

        return cmd

    def msg(self, *args, **kwargs):
        com.msg(*args, name=self.msgname, **kwargs)

    def msg2(self, *args, **kwargs):
        com.msg2(*args, name=self.msgname, **kwargs)

    def die(self, *args, **kwargs):
        com.die(*args, name=self.msgname, **kwargs)

    def fatal_error(self, *args, **kwargs):
        com.fatal_error(*args, name=self.msgname, **kwargs)

    def fail(self, *args, **kwargs):
        com.fail(*args, name=self.msgname, **kwargs)

    def error(self, *args, **kwargs):
        com.error(*args, name=self.msgname, **kwargs)

    def warning(self, *args, **kwargs):
        com.warning(*args, name=self.msgname, **kwargs)

    def debug(self, *args, **kwargs):
        com.debug(*args, name=self.msgname, **kwargs)

class Abuild:
    deps = abuild.deps.deps
    undeps = abuild.deps.undeps

    up2date = abuild.repo.up2date
    up2date_bool = abuild.repo.up2date_bool
    index = abuild.repo.index
    listpkg = abuild.repo.listpkg
    cleanpkg = abuild.repo.cleanpkg
    cleanoldpkg = abuild.repo.cleanoldpkg

    clean = abuild.setup.clean
    cleanup = abuild.setup.cleanup
    fetch = abuild.setup.fetch
    verify = abuild.setup.verify
    unpack = abuild.setup.unpack
    checksum = abuild.setup.checksum
    cleancache = abuild.setup.cleancache

    prepare = abuild.build.prepare
    mkusers = abuild.build.mkusers
    check = abuild.build.check

    rootpkg = abuild.package.rootpkg
    _prepare_all_pkg = abuild.package._prepare_all_pkg
    _create_archives = abuild.package._create_archives
    create_apks = abuild.package.create_apks

    rootbld = abuild.rootbld.rootbld
    _rootbld_actions = abuild.rootbld._rootbld_actions

    # pylint: disable=dangerous-default-value
    def __init__(self, filename="APKBUILD", env=os.environ, **kwargs):
        """
        .. class:: Abuild([filename="APKBUILD", env=os.environ])

           Create a new build object.

           :param filename:
              The filename to the APKBUILD that specifies this build. This can
              also be a directory, in which case ``"APKBUILD"`` will be
              appended.
           :type param: str or :class:`pathlib.Path`
           :param mapping settings:
              Settings for this build. See :attr:`.Abuild.settings`.
           :param mapping env:
              The environment for this build. The default is
              :data:`os.environ`.

           :param deps:
              Options for the dependency solver. Can be any value of
              :class:`.DepOpts`, the default being :attr:`.DepOpts.NONE`.
           :param dag:
              Options for the directed acyclic graph builder for the dependency
              solver. Can be any value of :class:`.DAGOpts`, the default being
              :attr:`.DAGOpts.YES`.
           :param bool force:
              Whether to force the build regardless of up2date status.
           :param str loglevel:
              The minimum threshold for log messages. Can be any of "DEBUG",
              "INFO", "WARNING", "ERROR", or "CRITICAL".
           :param bool keep_pkg:
              If ``True``, the ``up2date`` phase will only check for missing
              APKs, not if the APKBUILD or sources are newer.
           :param bool keep_tmp:
              If ``True``, the ``cleanup`` phase will not perform any action.
           :param bool pedantic:
              This option is passed to :class:`.APKBUILD`.
           :param bool mk_untrusted_index:
              When building the APKINDEX in the ``index`` phase, pass the
              ``--allow-untrusted`` option to ``apk``. This is primarily used
              for the test suite so that the example packager private key
              distributed with the source does not need to be installed to
              ``/etc/apk/keys`` in order for the test suite to pass.
           :param str root_nonce:
              Use the specified string as part of the buildroot path instead
              of a randomly generated nonce. The full path will be of the
              form ``"/var/tmp/abuild.{root_nonce}"``.
           :param bool upgrade_root:
              Whether to run update and upgrade the buildroot before
              building the package.
           :param dag_inst:
              An instance of :class:`.Digraph` for use with the dependency
              solver. This parameter is primarily used for recursive
              rebuilding.
           :param dict pkg_db:
              A dictionary mapping APKBUILD filenames to :class:`.Dependency`
              instances. This parameter is primarily used for recursive
              rebuilding.
        """
        # Settings
        self.file = Path(filename).expanduser().resolve()
        if not str(self.file).endswith("APKBUILD"):
            self.file /= "APKBUILD"

        #: .. attribute:: Abuild.settings
        #:
        #:    This is the dictionary of settings for the build.
        #:
        self.settings = {
            **{
                "deps": com.DepOpts.NONE,
                "dag": com.DAGOpts.YES,
                "force": False,
                "colors": False,
                "loglevel": "INFO",
                "keep_pkg": False,
                "keep_tmp": False,
                "pedantic": False,
                "mk_untrusted_index": False,
                "root_nonce": "",
                "upgrade_root": False,
                "dag_inst": None,
                "pkg_db": {},
            }, **kwargs}

        if "/" in self.settings["root_nonce"]:
            com.fatal_error("Nonce cannot contain '/'")

        self.dag = self.settings["dag_inst"]
        self.pkg_db = self.settings["pkg_db"]

        # Base directories
        #: .. attribute:: Abuild.env
        #:
        #:    The environment variables associated with this build. These
        #:    variables are passed to shell phases
        #:    (:meth:`.Abuild.shell_do_phase`). It is initialized as a copy of
        #:    the ``env`` argument to :class:`.Abuild` which defaults to
        #:    :data:`os.environ`. Example keys include ``startdir``,
        #:    ``srcdir``, ``pkgbasedir`` which are initialized on class
        #:    initialization.
        #:
        #:    .. todo:: This maybe should be passed to all children as well?
        #:
        #:    :INSERT:
        self.env = env.copy()
        self.env["startdir"] = self.file.parent.resolve()
        self.env["srcdir"] = self.env["startdir"] / "src"
        self.env["pkgbasedir"] = self.env["startdir"] / "pkg"
        #:    :INSERT:

        # Existence checks
        if not self.env["startdir"].is_dir():
            com.fatal_error("Could not find directory", self.env["startdir"])
        if not self.file.is_file():
            com.fatal_error("Could not find APKBUILD in", self.env["startdir"])

        # Global directories
        if com.unset_or_null(self.env, "REPODEST"):
            com.warning(
                "REPODEST is not set and is now required."
                f" Defaulting to {conf.REPODEST}")
            self.env["REPODEST"] = conf.REPODEST.expanduser()
        self.env["REPODEST"] = Path(self.env["REPODEST"])
        com.set_default(self.env, "SRCDEST", self.env["startdir"])
        self.env["SRCDEST"] = Path(self.env["SRCDEST"])
        self.env["repo"] = self.env["startdir"].parent.name
        self.repopath = self.env["REPODEST"] / self.env["repo"]

        # Subpackage directories
        com.set_default(self.env, "subpkgdir", "")
        com.set_default(self.env, "subpkgname", "")
        com.set_default(self.env, "subpkgarch", "")

        #: .. attribute:: Abuild.APKBUILD
        #:
        #:    This is the :class:`.APKBUILD` object associated with this build.
        #:    It is passed :attr:`.Abuild.env` as its ``env`` argument.
        self.APKBUILD = abuild.file.APKBUILD(
            self.file, env=self.env, pedantic=self.settings["pedantic"])
        self.virtual = f".makedepends-{self.pkgname}"
        self.virtual_installed = False
        self.buildroot = None

        # Package directories
        com.set_default(self.env, "builddir", self.env["srcdir"] / (
            f"{self.pkgname}-{self.pkgver}"))
        self.env["builddir"] = Path(self.env["builddir"])
        self.env["pkgdir"] = self.env["pkgbasedir"] / self.pkgname

        self.want_check = "!check" not in self.options
        if not self.settings["pedantic"]:
            self.want_check = "check" in self.APKBUILD.funcs and self.want_check

        # Subpackage defaults
        # TODO: also not cross_compiling and not ABUILD_BOOTSTRAP
        if not self.subpkgs_has_type("dbg"):
            if not com.unset_or_null(self.env, "DEFAULT_DBG"):
                if "!dbg" not in self.options and "noarch" not in self.arch:
                    # Add dbg subpackage to head of list so that symbols for
                    # all subpackages are built.
                    self.subpackages.insert(0, [self.pkgname + "-dbg", "dbg"])

        # Set debug compilation flags as appropriate
        if not com.unset_or_null(self.env, "DEBUG") or self.subpkgs_has_type("dbg"):
            self.env["CFLAGS"] += " -g"
            self.env["CXXFLAGS"] += " -g"
            self.options.append("!strip")

        # Git information
        self.commit = child.get_stdout_strip([
            self.env["GIT"], "log", "--format=%H", "--max-count=1",
            self.env["startdir"]])
        dirty = child.get_stdout_strip([
            self.env["GIT"], "status", "--porcelain",
            self.env["startdir"]])
        dirty = "-dirty" if dirty else ""
        self.commit = self.commit + dirty

        #: .. attribute:: Abuild.msgname
        #:
        #:    The logger name to use for this build. The default value is
        #:    ``abuild.`` plus the main package name. If the
        #:    :envvar:`FAKEROOTKEY` environment variable is not empty in
        #:    :attr:`.Abuild.env`, then a star is appended to the logger name.
        self.msgname = f"abuild.{self.pkgname}"
        if not com.unset_or_null(self.env, "ABUILD_BUILDROOT"):
            self.msgname += "@"
        if not com.unset_or_null(self.env, "FAKEROOTKEY"):
            self.msgname += "*"

        self.init_packages()

    def __getattr__(self, name):
        """
        .. method:: Abuild.__getattr__(name)

           Syntactic sugar to allow the usage of ``Abuild.name`` instead of
           ``Abuild.APKBUILD.meta["name"]``.
        """
        return self.APKBUILD.meta[name]

    def msg(self, *args, name=None, **kwargs):
        """
        .. method:: Abuild.msg(*args, **kwargs)
        """
        if not name:
            name = self.msgname
        com.msg(*args, name=name, **kwargs)

    def die(self, *args, **kwargs):
        """
        .. method:: Abuild.die(*args, **kwargs)
        """
        com.die(*args, name=self.msgname, **kwargs)

    def fatal_error(self, *args, **kwargs):
        com.fatal_error(*args, name=self.msgname, **kwargs)

    def fail(self, *args, **kwargs):
        com.fail(*args, name=self.msgname, **kwargs)

    def error(self, *args, **kwargs):
        """
        .. method:: Abuild.error(*args, **kwargs)
        """
        com.error(*args, name=self.msgname, **kwargs)

    def warning(self, *args, **kwargs):
        """
        .. method:: Abuild.warning(*args, **kwargs)
        """
        com.warning(*args, name=self.msgname, **kwargs)

    def debug(self, *args, name=None, **kwargs):
        """
        .. method:: Abuild.debug(*args, **kwargs)

           These are convenience methods that will pass :attr:`.Abuild.msgname`
           to the appropriate logging function.
        """
        if not name:
            name = self.msgname
        com.debug(*args, name=name, **kwargs)

    def subpkgs_has_type(self, t):
        # UNDOC
        return any([name[0].endswith("-" + t) for name in self.subpackages])

    def init_packages(self):
        # UNDOC
        self.allarches = set()
        self.allpackages = {}

        if com.unset_or_null(self.env, "pkgarch"):
            if "noarch" in self.arch:
                self.pkgarch = "noarch"
            elif "!" + self.env["CARCH"] in self.arch:
                self.fatal_error(
                    "Package not available for the target architecture"
                    f" ({self.env['CARCH']}). Aborting.")
            elif "all" not in self.arch and self.env["CARCH"] not in self.arch:
                self.fatal_error(
                    "Package not available for the target architecture"
                    f" ({self.env['CARCH']}). Aborting.")
            else:
                self.pkgarch = self.env["CARCH"]
        else:
            self.pkgarch = self.env["pkgarch"]

        if self.pkgarch == "noarch":
            self.allarches.add(self.env["CARCH"])
        else:
            self.allarches.add(self.pkgarch)

        for subpkg in self.subpackages:
            i = len(subpkg)
            if i == 1:
                subpkg.append("")
                subpkg.append("")
            if i == 2:
                subpkg.append("")
            name = subpkg[0]
            split = subpkg[1]
            arch = subpkg[2]

            if not arch:
                if com.glob_match(name, conf.DEFAULT_NOARCH):
                    arch = subpkg[2] = "noarch"
                else:
                    arch = subpkg[2] = self.pkgarch
            if arch != "noarch":
                self.allarches.add(arch)

            self.allpackages[name] = Subpackage(name, split, arch, self, self.env)
            self.reset_capture(self.allpackages[name])

        name = self.pkgname
        split = "package"
        arch = self.pkgarch
        self.allpackages[name] = Subpackage(name, split, arch, self, self.env)
        self.reset_capture(self.allpackages[name])

    def reset_capture(self, pkg):
        # UNDOC
        pkg.capture = {}
        for var in conf.SUBPKG_VARS:
            if conf.SUBPKG_VARS[var]:
                default_name = conf.SUBPKG_VARS[var]
                default = self.APKBUILD.meta[default_name]
                pkg.capture[var] = default
            else:
                pkg.capture[var] = conf.SUBPKG_VARS[var]
        pkg.capture["arch"] = []

    def do_phase(self, phases, capture=False):
        """
        .. method:: Abuild.do_phase(phases, [capture=False])

           Perform the specified phase(s). This function will in turn call
           either the appropriate method of the :class:`.Abuild` class, or call
           :meth:`.Abuild.shell_do_phase`. For most purposes it is recommended
           to call this method to perform a phase instead of any other because
           it performs verification of the phase name.

           :param phase:
               The name of the phase as a string for a single phase, or an
               iterable of phase name strings.
           :type param: iterable or str
           :param capture:
               This is only useful for shell phases and is passed to
               :meth:`.Abuild.shell_do_phase`.
        """
        if isinstance(phases, str):
            phases = [phases]

        for phase in phases:
            self.debug(f"Starting phase {phase}")
            if phase.startswith("default_"):
                default = True
                phase = phase.replace("default_", "")
            elif phase == "check":
                default = True
            else:
                default = False

            all_available_phases = conf.AVAILABLE_PHASES \
                + tuple(self.APKBUILD.funcs.keys()) \
                + conf.FUNCS_WITH_DEFAULT
            if phase not in all_available_phases:
                self.fatal_error(
                    "Not a valid phase or APKBUILD function:", phase)

            if phase in conf.BUILDDIR_PHASES and self.env["builddir"].exists():
                os.chdir(self.env["builddir"])

            if phase in self.APKBUILD.funcs and not default:
                phase_method = self.shell_do_phase

            elif phase in conf.SHELL_PHASES:
                phase_method = self.shell_do_phase

            else:
                phase_method = self.__getattribute__(phase)

            try:
                # pylint: disable=comparison-with-callable
                if not phase_method or phase_method == self.shell_do_phase:
                    self.shell_do_phase(phase, capture=capture)
                else:
                    phase_method()

            except exc.abuildException:
                self.error(phase, "failed")
                raise

    def shell_do_phase(self, phase, capture=False):
        """
        .. method:: Abuild.shell_do_phase(phase, [capture=False])

           Drop into a shell to perform a phase.

           :param str phase:
               The name of the phase to execute. If there was no function
               detected for this phase previously, the "default_*" version of
               the phase will be executed instead.
           :param capture:
               If given as a :class:`dict`, the variables will be stored there.
        """
        if phase not in self.APKBUILD.funcs:
            phase = "default_" + phase

        if isinstance(capture, dict):
            args = ["sh", "-ea", conf.PHASE_PATH, self.APKBUILD.file, phase]
            prefix = f"abuild-{os.getpid()}-"
            tmp = tempfile.NamedTemporaryFile(
                mode="r", prefix=prefix, suffix=".env")
            args.append(tmp.name)
            res = child.get_retcode(args, env=self.env, retcodes="all")
            if res != 0:
                raise exc.abuildFailure(
                    f"Shell phase {phase} failed with status {res}")
            self.save_env(tmp, capture)
            tmp.close()
        else:
            args = [conf.PHASE_PATH, self.APKBUILD.file, phase]
            res = child.get_retcode(args, env=self.env, retcodes="all")
            if res != 0:
                raise exc.abuildFailure(
                    f"Shell phase {phase} failed with status {res}")

    def save_env(self, tmp, capture):  # pylint: disable=no-self-use
        """
        .. method:: Abuild.save_env(capture, tmp)

           Save the environment variables stored in the file-like object at
           ``tmp`` into the ``capture`` argument.
        """
        output = tmp.read()
        for line in output.split("\0"):
            line = line.replace("\t", " ")
            assignment = conf.RE_ASSIGNMENT.fullmatch(line)
            if assignment:
                assignment = assignment.groups()
                name = assignment[0]
                value = assignment[1]

                if name not in conf.SUBPKG_VARS:
                    continue

                if name in conf.GENERAL_PROPERTIES:
                    capture[name] = value

                elif name in conf.SPLITTABLE_PROPERTIES:
                    capture[name] = value.split()

                    if "depends" in name:
                        capture[name] = set(capture[name])

    def do_fakeroot(self, phases):
        # UNDOC
        if (com.unset_or_null(self.env, "FAKEROOT")
                or not com.unset_or_null(self.env, "FAKEROOTKEY")):
            self.do_phase(phases)

        else:
            self.msg("Entering fakeroot...")
            args = [self.env["FAKEROOT"], "--", self.env["ABUILD"]]
            args.extend(self.env["ABUILD_OPTS"].split())
            args.extend(phases)
            child.get_retcode(args)

    def all(self):
        """
        .. method:: Abuild.all()

           (default) If not up2date, run all of the default phases

           The default phases are determined by :data:`.DEFAULT_PHASES`.
        """
        if "!libc_" + self.env["CLIBC"] in self.options:
            self.fatal_error(
                "Package not available for the target libc"
                f" ({self.env['CLIBC']}). Aborting")

        if self.up2date_bool():
            self.msg("Package is up to date.")
            return

        self.do_phase(conf.DEFAULT_PHASES)
