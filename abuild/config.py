# SPDX-License-Identifier: GPL-2.0-only
# Copyright (c) 2018 Max Rees
# See LICENSE for more information.
"""
Module abuild.config
====================

.. module:: abuild.config
   :synopsis: Default configuration values for the abuild package.

This module contains constants that are used throughout the py3-abuild package.
These typically should only be modified by packagers and not the end user.
"""
from pathlib import Path
import re         # compile, VERBOSE, MULTILINE, DOTALL

# UNDOC
PROG_NAME = "py3-abuild"
# UNDOC
PROG_VER = "0.0.1"
# UNDOC
HELPERS_PATH = Path(__file__).parent / "helpers"
# UNDOC
PHASE_PATH = HELPERS_PATH / "phase.sh"
# UNDOC
SHELLEXPAND_PATH = HELPERS_PATH / "shellexpand.sh"
# UNDOC
ORIGINS_PATH = HELPERS_PATH / "origins.sh"
#: .. data:: ABUILD_CONF
#:
#:    The default path to the system abuild configuration file.  If the
#:    environment variable :envvar:`ABUILD_CONF` is set, that value will take
#:    precedence over the default value defined here as ``/etc/abuild.conf``.
#:
ABUILD_CONF = Path("/etc/abuild.conf")
#: .. data:: ABUILD_USERDIR
#:
#:    The default user directory to look in for the user's personal
#:    ``abuild.conf``. If the environment variable :envvar:`ABUILD_USERDIR`
#:    is set, that value will take precedence over the default value defined
#:    here as ``~/.abuild``.
#:
ABUILD_USERDIR = Path("~/.abuild")
#: .. data:: ABUILD_USERCONF
#:
#:    The default filename for the user's personal ``abuild.conf``. If the
#:    environment variable :envvar:`ABUILD_USERCONF` is set, that value will
#:    take precedence over the default value defined here as
#:    ``$ABUILD_USERDIR/abuild.conf``.
#:
ABUILD_USERCONF = ABUILD_USERDIR / "abuild.conf"
#: .. data:: REPODEST
#:
#:     The default directory in which to place built packages. If the
#:     environment variable :envvar:`REPODEST` is not set, a warning will
#:     be generated and the default value, defined here as ``~/packages``,
#:     will be used.
#:
REPODEST = Path("~/packages")
#: .. data:: NORMAL
#: .. data:: STRONG
#:
#:    These are string constants that contain the terminal escape codes for
#:    reset/normal and bold/strong.
#:
#: .. data:: RED
#: .. data:: GREEN
#: .. data:: YELLOW
#: .. data:: BLUE
#:
#:    These are string constants that contain the terminal escape codes for
#:    their respective colors.
NORMAL = "\033[1;0m"
STRONG = "\033[1;1m"
RED = "\033[1;31m"
GREEN = "\033[1;32m"
YELLOW = "\033[1;33m"
BLUE = "\033[1;34m"
MAGENTA = "\033[1;35m"
#: .. data:: COLORS
#:
#:    This is a mapping of log levels to colors. It is used if the
#:    :envvar:`USE_COLORS` environment variable is not null and the ``-m``
#:    command line option was not given, or if the ``-c`` command line option
#:    was given.
#:
#:    .. note:: By default, the :class:`.abuild` class does not use colors,
#:              but the command line program will.
#:
#:    :INSERT:
COLORS = {
    "CRITICAL": RED,
    "ERROR":    RED,
    "WARNING":  YELLOW,
    "INFO":     GREEN,
    "DEBUG":    BLUE,
}
#:    :INSERT:
#:
#: .. data:: REQUIRED_PROPERTIES
#:
#:    This is the list of properties that an APKBUILD *must* have. If the
#:    APKBUILD does not have one of the properties listed in
#:    REQUIRED_PROPERTIES, it is a fatal error.
#:
#:    :INSERT:
REQUIRED_PROPERTIES = (
    "pkgname",
    "pkgver",
    "pkgrel",
    "pkgdesc",
    "url",
    "license",
    "arch",
    # "source", # user/gtk+ has no source?
)
#:    :INSERT:
#:
#: .. data:: GENERAL_PROPERTIES
#:
#:    This is the list of APKBUILD properties that are stored internally as a
#:    string.
#:
#:    :INSERT:
GENERAL_PROPERTIES = (
    "pkgname",
    "pkgver",
    "pkgrel",
    "pkgdesc",
    "url",
    "license",
    "provider_priority",
    "replaces_priority",
    "builddir",
    "giturl",
    "ldpath",
    "sonameprefix",
)
#:    :INSERT:
#:
#: .. data:: SPLITTABLE_PROPERTIES
#:
#:    This is the list of APKBUILD properties that are stored internally as a
#:    ``list``. The ``*depends*`` properties are stored as a ``set`` instead of
#:    a ``list`` in order to deduplicate the entries quickly.
#:
#:    :INSERT:
SPLITTABLE_PROPERTIES = (
    "arch",
    "checkdepends",
    "depends",
    "depends_dev",
    "install",
    "install_if",
    "makedepends",
    "makedepends_build",
    "makedepends_host",
    "pkggroups",
    "pkgusers",
    "provides",
    "options",
    "replaces",
    "source",
    "triggers",
    "somask",
)
#:    :INSERT:
#:
#: .. data:: CHECKSUMS_READ
#:
#:    This is the list of APKBUILD ``source`` verification properties that will
#:    be read from the APKBUILD. In the past, there used to be ``md5sums`` and
#:    ``sha256sums``, but typically only ``sha512sums`` is used now.
#:
#:    :INSERT:
CHECKSUMS_READ = (
    "md5sums",
    "sha256sums",
    "sha512sums",
)
#:    :INSERT:
#:
#: .. data:: CHECKSUMS_WRITE
#:
#:    This is the list of APKBUILD ``source`` verification properties that will
#:    be written to the APKBUILD if the ``checksum`` command is used.
#:
#;    .. seealso :data:`.CHECKSUMS_READ`
#:
#:    :INSERT:
CHECKSUMS_WRITE = (
    "sha512sums",
)
#:    :INSERT:
#:
#: .. data:: FUNCS_WITH_DEFAULT
#:
#:    This is the list of subpackages that have a default split function.
#:
#:    :INSERT:
FUNCS_WITH_DEFAULT = (
    "dbg",
    "dev",
    "doc",
    "lang",
    "libs",
    "openrc",
)
#:    :INSERT:
#:
#: .. data:: DEFAULT_NOARCH
#:
#:    This is a list of globs of packages that should default to
#:    ``arch=noarch``.  It's typically used to assign the ``arch`` property of
#:    subpackages when analyzing an APKBUILD.
#:
#:    :INSERT:
DEFAULT_NOARCH = (
    "*-doc",
    "*-lang",
    "*-lang-*",
    "*-openrc",
)
#:    :INSERT:
#:
#: .. data:: AVAILABLE_PHASES
#:
#:    This is the master list of phases that are currently supported. If
#:    :meth:`.abuild.do_phase` is called with a phase that is not in
#:    :data:`AVAILABLE_PHASES` or in :data:`.APKBUILD.funcs`, an
#:    :exc:`.abuildError` is raised.
#:
#:    :INSERT:
AVAILABLE_PHASES = (
    "all",
    "up2date",
    "deps",
    "undeps",
    "clean",
    "fetch",
    "verify",   # will call fetch
    "unpack",   # will call verify
    "prepare",
    "mkusers",
    "build",
    "check",
    "rootpkg",  # will call package, _prepare_all_pkg, _create_archives, create_apks
    "package",
    "_prepare_all_pkg",
    "_create_archives",
    "create_apks",
    "index",
    "cleanup",

    "cleancache",
    "checksum", # will call fetch
    "listpkg",
    "cleanpkg",
    "cleanoldpkg",

    "rootbld",
    "_rootbld_actions",
)
#:    :INSERT:
#:
#: .. data:: CHECK_REQUIRED
#:
#:    Whether or not to enforce provision of either a ``check()`` APKBUILD
#:    function or ``options="!check"``. If ``True``, and the APKBUILD does not
#:    have either, it is a fatal error. The default is ``False``. A ``check()``
#:    function is required regardless of this setting if pedantic mode is
#:    enabled.
#:
CHECK_REQUIRED = False
#:
#: .. data:: DEFAULT_PHASES
#:
#:    The list of phases to perform if :meth:`.abuild.all` is called.
#:
#:    :INSERT:
DEFAULT_PHASES = (
    "deps",
    "clean",
    "unpack",
    "prepare",
    "mkusers",
    "build",
    "check",
    "rootpkg",
    "index",
    "cleanup",
)
#:    :INSERT:
#:
#: .. data:: SHELL_PHASES
#:
#:    The list of phases that need to be performed in the shell.
#:
#:    :INSERT:
SHELL_PHASES = (
    "build",
    "package",
    *FUNCS_WITH_DEFAULT,
)
#:    :INSERT:
#:
#: .. data:: BUILDDIR_PHASES
#:
#:    The list of phases that need to be performed in $builddir.
#:
#:    :INSERT:
BUILDDIR_PHASES = (
    "prepare",
    "build",
    "check",
    "package",
)
#:    :INSERT:
#:
#: .. data:: ARCH_TO_HOSTSPEC
#:
#:    A mapping from ``arch`` to ``hostspec`` (also known as a target triplet).
#:    This is used in the :func:`.arch_to_hostspec` function.
#:
#:    :INSERT:
ARCH_TO_HOSTSPEC = {
    "aarch64":  "aarch64-foxkit-linux-musl",
    "armel":    "armv5-foxkit-linux-musleabi",
    "armhf":    "armv6-foxkit-linux-muslgnueabihf",
    "armv7":    "armv7-foxkit-linux-musleabihf",
    "i528":     "pentium4-foxkit-linux-musl",
    "mips":     "mips-foxkit-linux-musl",
    "mipsel":   "mipsel-foxkit-linux-musl",
    "mips32":   "mips32-foxkit-linux-musl",
    "mips32el": "mips32el-foxkit-linux-musl",
    "pmmx":     "i586-foxkit-linux-musl",
    "ppc":      "powerpc-foxkit-linux-musl",
    "ppc64":    "powerpc64-foxkit-linux-musl",
    "ppc64le":  "powerpc64le-foxkit-linux-musl",
    "s390x":    "s390x-foxkit-linux-musl",
    "x86":      "i486-foxkit-linux-musl",
    "x86_64":   "x86_64-foxkit-linux-musl",
}
#:    :INSERT:
#:
#: .. data:: HOSTSPEC_TO_ARCH
#:
#:    A glob mapping from ``hostspec`` to ``arch``. This is used by the
#:    :func:`.hostspec_to_arch` function.
#:
#:    :INSERT:
HOSTSPEC_TO_ARCH = {
    "aarch64*-*-*-*":     "aarch64",
    "arm*-*-*-*eabi":     "armel",
    "armv6*-*-*-*eabihf": "armhf",
    "armv7*-*-*-*eabihf": "armv7",
    "i486-*-*-*":         "x86",
    "i586-*-*-*":         "pmmx",
    "mips-*-*-*":         "mips",
    "mipsel-*-*-*":       "mipsel",
    "mips32-*-*-*":       "mips32",
    "mips32el-*-*-*":     "mips32el",
    "pentium4-*-*-*":     "i528",
    "powerpc-*-*-*":      "ppc",
    "powerpc64-*-*-*":    "ppc64",
    "powerpc64le-*-*-*":  "ppc64le",
    "s390x-*-*-*":        "s390x",
    "x86_64-*-*-*":       "x86_64",
}
#:    :INSERT:
#:
#: .. data:: HOSTSPEC_TO_LIBC
#:
#:    A glob mapping from ``hostspec`` to ``libc``. This is used by the
#:    :func:`.hostspec_to_libc` function.
#:
#:    :INSERT:
HOSTSPEC_TO_LIBC = {
    "*-*-*-uclibc*": "uclibc",
    "*-*-*-musl*":   "musl",
    "*-*-*-gnu*":    "glibc",
}
#:    :INSERT:
#:
#: .. data:: SUBPKG_VARS
#:
#:    A list of properties that may be modified from within a subpackage split
#:    function. This is used by the :meth:`.abuild.save_env` method. The values
#:    correspond to the default values: if not the empty string, the default
#:    value will be inherited from the origin package.
#:
#:    :INSERT:
SUBPKG_VARS = {
    "depends":           "depends",
    "depends_dev":       "depends_dev",
    "install":           "install",
    "install_if":        [],
    "license":           "license",
    "pkgdesc":           "pkgdesc",
    "provides":          [],
    "provider_priority": "",
    "replaces":          [],
    "replaces_priority": "",
    "triggers":          "triggers",
    "url":               "url",
    "options":           "options",
    "sonameprefix":      "sonameprefix",
    "somask":            "somask",
    "ldpath":            "ldpath",
}
#:    :INSERT:
# UNDOC
RE_ASSIGNMENT = re.compile(r"""
    ^([a-zA-Z_]       # First letter of variable name
    [a-zA-Z0-9_]*)    # Rest of variable name
    [=]               # Literal "="
    (.*)$             # Match anything to end of line
    """, re.MULTILINE | re.DOTALL | re.VERBOSE)
# UNDOC
RE_FUNCHEAD = re.compile(r"""
    ^\s*([a-zA-Z_]    # First letter of function name
    [a-zA-Z0-9_]*)    # Rest of function name
    \s*\(\)\s*        # Literal "()" surrounded by whitespace
    {$                # Opening brace""", re.VERBOSE)
# UNDOC
RE_FUNCTAIL = re.compile(r"^}$")
# UNDOC
RE_FUNCINLINE = re.compile(r"""
    ^\s*([a-zA-Z_]    # First letter of function name
    [a-zA-Z0-9_]*)    # Rest of function name
    \s*\(\)\s*        # Literal "()" surrounded by whitespace
    {(.*)}$           # Function body""", re.VERBOSE)
# UNDOC
RE_EMAILS = re.compile(r"""
    ^[#][ ]           # Literal "# "
    (Contributor|Maintainer):
    \s*(.*)?$         # Match anything to end of line""", re.VERBOSE)
