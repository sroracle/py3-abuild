# SPDX-License-Identifier: GPL-2.0-only
# Copyright (c) 2018 Max Rees
# See LICENSE for more information.
import tempfile # mkdtemp
import shutil   # copy2
import glob     # iglob
import logging  # getLogger
from pathlib import Path

import abuild.child as child
import abuild.deps as deps

_MAKE_DIRS = [
    "{BUILDROOT}/etc",
    "{BUILDROOT}/proc",
    "{BUILDROOT}/{HOME}",
]

def create_bldroot(self):
    self.msg(f"Creating buildroot {self.buildroot}...")

    # Normally we would be concerned that these files in /etc will be modifiable
    # by the same user that the build runs under, but /etc will be mounted
    # readonly (as /) inside of the buildroot.
    for directory in _MAKE_DIRS:
        directory = Path(directory.format(**self.env, BUILDROOT=self.buildroot))
        directory.mkdir(parents=True, exist_ok=True)

    shutil.copy2("/etc/group", self.buildroot / "etc/group")
    shutil.copy2("/etc/passwd", self.buildroot / "etc/passwd")

    (self.buildroot / "etc/apk/keys").mkdir(parents=True, exist_ok=True)
    for key in glob.iglob("/etc/apk/keys/*"):
        shutil.copy2(key, self.buildroot / "etc/apk/keys")

    if "net" in self.options:
        shutil.copy2("/etc/resolv.conf", self.buildroot / "etc")

    abuild_conf = self.env["ABUILD_CONF"]
    abuild_userconf = self.env["ABUILD_USERCONF"]
    if abuild_conf and Path(abuild_conf).is_file():
        userdir = self.buildroot / self.env["ABUILD_USERDIR"].lstrip("/")
        userdir.mkdir(parents=True, exist_ok=True)
        shutil.copy2(abuild_conf, self.buildroot / abuild_conf.lstrip("/"))
    if abuild_userconf and Path(abuild_userconf).is_file():
        Path(abuild_userconf).parent.mkdir(parents=True, exist_ok=True)
        shutil.copy2(abuild_userconf, self.buildroot / abuild_userconf.lstrip("/"))

    template = Path(self.env["APORTSDIR"]) / self.env["repo"]
    template /= ".rootbld-repositories"
    if not template.is_file():
        self.fatal_error(f"{template} does not exist")
    shutil.copy2(template, self.buildroot / "etc/apk/repositories")

    args = [self.env["SUDO_APK"], "add", "--initdb", "--root", self.buildroot]
    args += ["--update", "py3-abuild", "adelie-base", "build-tools", "git"]
    child.run_child(args, env=self.env)

def _rootbld_actions(self):
    self.do_phase(["unpack", "prepare", "build", "check"])
    self.do_fakeroot(["package", "_prepare_all_pkg", "_create_archives"])

def rootbld(self):
    if self.up2date_bool():
        self.msg("Package is up to date")
        return

    if self.env["CBUILD"] != self.env["CHOST"]:
        self.fatal_error("rootbld does not support cross-building")

    self.do_phase(["clean", "verify", "mkusers"])

    if self.settings["root_nonce"]:
        self.buildroot = "/var/tmp/abuild."
        self.buildroot += self.settings["root_nonce"]
        self.buildroot = Path(self.buildroot)
    else:
        self.buildroot = Path(
            tempfile.mkdtemp(prefix="abuild.", dir="/var/tmp"))
        self.settings["root_nonce"] = self.buildroot.name.replace(
            "abuild.", "", 1)

    create_bldroot(self)

    if self.settings["upgrade_root"]:
        self.msg("Upgrading buildroot...")
        child.run_child([
            self.env["SUDO_APK"], "--root", self.buildroot,
            "update"])
        child.run_child([
            self.env["SUDO_APK"], "--root", self.buildroot,
            "upgrade", "--available"])

    add_deps = deps.resolve_deps(self, main=True)
    if add_deps:
        deps.install_deps(self, add_deps)

    bwrap_args = [
        "bwrap", "--unshare-ipc", "--unshare-uts", "--unshare-user",
        "--unshare-cgroup", "--unshare-pid", "--new-session",
        "--die-with-parent", "--hostname", self.settings["root_nonce"],
    ]
    if "net" not in self.options:
        bwrap_args.append("--unshare-net")

    home = self.env["HOME"]
    self.msg(f"Entering buildroot {self.buildroot}...")

    bwrap_args += [
        "--ro-bind", self.buildroot, "/",
        "--proc", "/proc",
        "--dev-bind", "/dev", "/dev",
        "--bind", self.buildroot / "tmp", "/tmp",
        "--bind", self.buildroot / home.lstrip("/"), home,
        "--bind", self.env["SRCDEST"], self.env["SRCDEST"],
        "--bind", self.env["APORTSDIR"], self.env["APORTSDIR"],
        "--bind", self.env["REPODEST"], self.env["REPODEST"],
    ]

    logger = logging.getLogger("abuild")
    if hasattr(logger, "logfile") and hasattr(logger.logfile, "name"):
        bwrap_args += ["--bind", logger.logfile.name, logger.logfile.name]

    bwrap_args += [
        "--chdir", self.env["startdir"],
        "--setenv", "PATH", "/bin:/usr/bin:/sbin:/usr/sbin",
        "--setenv", "ABUILD_BUILDROOT", "1",
        self.env["ABUILD"], *self.env["ABUILD_OPTS"].split(),
        "_rootbld_actions"
    ]
    child.run_child(bwrap_args)

    self.msg("Exiting buildroot")
    self.do_phase(["create_apks", "index", "cleanup"])
