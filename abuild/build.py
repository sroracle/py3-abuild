# SPDX-License-Identifier: GPL-2.0-only
# Copyright (c) 2018 Max Rees
# See LICENSE for more information.

import abuild.common as com
import abuild.child as child

def prepare(self):
    """
    .. method:: Abuild.prepare()

       Apply patches and perform other pre-build steps

       Note that the patches are taken from ``srcdir`` and applied from
       ``builddir``, so the unpack phase must be completed first.
    """
    if not any(com.filename_from_uri(i).endswith(".patch") for i in self.source):
        return

    if not self.env["builddir"].is_dir():
        self.fatal_error(
            "$builddir doesn't seem to exist..."
            " Try running unpack first?")

    args = ["patch", "-p1", "-i", "<src>"]
    for src in self.source:
        if src.endswith(".patch"):
            self.msg("Applying", src)
            args[3] = str(self.env["srcdir"] / src)
            child.get_retcode(args)

def mkusers(self):
    """
    .. method:: Abuild.mkusers()

       Add the groups and users from $pkggroups and $pkgusers

       The ADDGROUP and ADDUSER environment variables control what commands
       are used to perform this phase.
    """
    for group in self.pkggroups:
        if not com.safe_getgrnam(group):
            self.msg("Creating group", group)
            child.get_retcode([self.env["ADDGROUP"], "-S", group])

    for user in self.pkgusers:
        if not com.safe_getpwnam(user):
            self.msg("Creating user", user)
            args = [self.env["ADDUSER"], "-S", "-D", "-H"]

            if com.safe_getgrnam(user):
                args.extend(["-G", user])

            args.append(user)
            child.get_retcode(args)

def check(self):
    """
    .. method:: Abuild.check()

       Run package tests if provided
    """
    if self.want_check:
        if ("checkroot" in self.options
                and not com.unset_or_null(self.env, "FAKEROOT")
                and com.unset_or_null(self.env, "FAKEROOTKEY")):
            self.do_fakeroot(["check"])
        else:
            self.shell_do_phase("check")
