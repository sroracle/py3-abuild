# SPDX-License-Identifier: GPL-2.0-only
# Copyright (c) 2018 Max Rees
# See LICENSE for more information.
from pathlib import Path
from enum import Flag

import abuild  # pylint: disable=cyclic-import
from abuild.digraph import Digraph
import abuild.config as conf
import abuild.common as com
import abuild.child as child
import abuild.file

class Visited(Flag):
    NONE = 0
    FILE = 1
    DEPS = FILE | 2

class Dependency:
    def __init__(self, name, filename):
        self.name = name
        self.file = filename

        self.visited = Visited.NONE
        self.abuild = None

def analyze_origins(pkgs):
    """
    .. function:: analyze_origins(pkgs)

       Populate the ``pkgs`` dictionary with package name -> APKBUILD file
       location pairs.
    """
    origins = child.get_stdout([conf.ORIGINS_PATH])

    for entry in origins.splitlines():
        entry = entry.split("\t")
        filename = Path(entry[1])
        if entry[0] not in pkgs:
            pkgs[entry[0]] = Dependency(entry[0], filename)

        if entry[2]:
            pkgs[entry[2]] = pkgs[entry[0]]

def check_installed(apk_path, pkgs):
    """
    .. function:: check_installed(pkgs)

       Check each package in a list to see what's installed.

       :param str apk_path:
           The name of the ``apk`` executable.

       :param set pkgs:
           The set of packages to analyze. If a package's name starts with a
           bang ("!"), the function will die if that package is installed.

       :returns set: A set of packages that are not installed.
    """
    maybe_installed = set()
    missing = set()

    for pkg in pkgs:
        if pkg.startswith("!"):
            # When given "!pkg", APK will return 1 if pkg is installed
            # and 0 otherwise
            installed = child.get_retcode(
                [apk_path, "info", "--quiet", "--installed", pkg],
                retcodes=[1])
            if installed:
                com.fatal_error(
                    "conflicting package installed:",
                    pkg.lstrip("!"))
        else:
            maybe_installed.add(pkg)

    args = [apk_path, "info", "--installed"]
    args.extend(maybe_installed)
    installed = child.get_stdout(args, retcodes="all")
    installed = set(installed.splitlines())
    missing = maybe_installed - installed

    return missing

def resolve_build_deps(ab, depslist, main):
    # UNDOC
    if main:
        analyze_origins(ab.pkg_db)
        ab.dag = ab.settings["dag_inst"] = Digraph(cyclic_fatal=False)
        ab.dag.firstcycle = True

    needs_build = set()
    needs_install = set()

    for dep in depslist:
        clean_dep = com.atomize(dep)
        if clean_dep.startswith(("cmd:", "!")):
            ab.debug(f"Skipping build for dependency: {dep}")
            needs_install.add(dep)
            continue

        if (not clean_dep in ab.pkg_db
                or not ab.pkg_db[clean_dep].file.is_file()):
            msg = f"Could not find APKBUILD for {clean_dep}"
            if ab.settings["dag"] == com.DAGOpts.DOT:
                ab.warning(msg)
            else:
                ab.fatal_error(msg)

            needs_build.add(clean_dep)
            ab.dag.missing.add(clean_dep)
            continue

        if (ab.pkg_db[clean_dep].visited & Visited.FILE
                and ab.pkg_db[clean_dep].abuild):
            ab_dep = ab.pkg_db[clean_dep].abuild
        else:
            ab_dep = abuild.Abuild(ab.pkg_db[clean_dep].file, **ab.settings)
            ab.pkg_db[clean_dep].abuild = ab_dep
            ab.pkg_db[clean_dep].visited |= Visited.FILE

        if ab_dep.env["repo"] != ab.env["repo"]:
            ab.debug(
                "Skipping build for dependency:"
                f" {ab_dep.env['repo']}/{dep}")
            needs_install.add(dep)
        elif not ab_dep.up2date_bool():
            needs_build.add(ab_dep)

    return (needs_build, needs_install)

def resolve_deps(ab, main):
    # UNDOC
    if main:
        ab.msg("Analyzing dependencies...")
    new_deps = ab.makedepends
    new_deps |= ab.depends

    if ab.want_check:
        new_deps |= ab.checkdepends

    add_deps = set()
    for pkg in new_deps:
        _pkg = com.atomize(pkg)
        if _pkg in ab.allpackages:
            ab.debug("Ignoring direct cyclic dependency:", _pkg)
            continue

        add_deps.add(pkg)

    return add_deps

def resolve_build_dag(ab, depslist, main):
    # UNDOC
    dot = ab.settings["dag"] == com.DAGOpts.DOT

    ab.dag.add_node_if_not_exists(ab.pkgname)
    for abuild_dep in depslist:
        missing = isinstance(abuild_dep, str)
        name = abuild_dep if missing else abuild_dep.pkgname

        ab.dag.add_node_if_not_exists(name)

        if not ab.dag.add_edge(name, ab.pkgname):
            if ab.dag.firstcycle:
                # If we're building a DOT representation of the dependency
                # graph, we don't want to exit even if cyclic dependencies are
                # detected. Instead, we'll just print a warning the first time
                # one is detected.
                ab.dag.firstcycle = False
                msg = f"Cyclic dependency detected: {ab.pkgname} -> {name}"
                if dot:
                    ab.warning(msg)
                # Otherwise, we should bail out immediately.
                else:
                    ab.fatal_error(msg)

        if not missing:
            if ab.pkg_db[abuild_dep.pkgname].visited != Visited.DEPS:
                ab.pkg_db[abuild_dep.pkgname].visited = Visited.DEPS
                abuild_dep.deps(main=False)

    if main:
        if dot:
            ab.dag.dot()
        else:
            build_order = ab.dag.topological_sort()
            # We don't want to build ourselves twice
            build_order.remove(ab.pkgname)
            return build_order

    return None

def build_deps(ab, needs_build):
    # UNDOC

    for dep in needs_build:
        if isinstance(dep, str):
            abuild_dep = ab.pkg_db[dep].abuild
        else:
            abuild_dep = dep

        if not abuild_dep.up2date_bool():
            abuild_dep.settings["dag"] = com.DAGOpts.NO
            abuild_dep.all()

def install_deps(ab, depslist):
    # UNDOC
    if not depslist:
        return

    ab.msg("Installing for build:", " ".join(depslist))
    args = [ab.env["SUDO_APK"], "add"]
    if ab.buildroot:
        args += ["--root", ab.buildroot]

    args += ["--repository", ab.repopath, "--virtual", ab.virtual]
    args += depslist

    child.get_retcode(args, env=ab.env)
    ab.virtual_installed = True

def deps(self, main=True):
    """
    .. method:: abuild.deps([main=True])

       Install/build packages in makedepends and depends

       If the ``-r`` option was given, then the dependencies will be
       installed from the default repositories. If the ``-R`` option was
       given, then the dependencies that are not already built will be
       recursively built and installed.
    """
    dep_opts = self.settings["deps"]
    dag_opts = self.settings["dag"]

    if dep_opts & com.DepOpts.IGNORE:
        return

    add_deps = resolve_deps(self, main)

    if not add_deps:
        return

    if dep_opts & com.DepOpts.BUILD or dag_opts:
        needs_build, _unused = resolve_build_deps(self, add_deps, main)

    if dag_opts & (com.DAGOpts.YES | com.DAGOpts.ONLY):
        needs_build = resolve_build_dag(self, needs_build, main)

        if main and needs_build:
            self.msg(f"Building for build:", " ".join(needs_build))

        # Sometimes that's all we need to do...
        if not main or dag_opts & com.DAGOpts.ONLY:
            return

    elif dep_opts & com.DepOpts.BUILD and needs_build:
        self.msg(
            "Building for build:",
            " ".join(pkg.pkgname for pkg in needs_build))

    if dep_opts & com.DepOpts.BUILD:
        build_deps(self, needs_build)

    if dep_opts & (com.DepOpts.INSTALL | com.DepOpts.BUILD):
        # Even if all of add_deps is already installed, let's do this
        # anyway.  We don't want to get clobbered by another build
        # running "undeps".
        install_deps(self, add_deps)

    else:
        need_deps = check_installed(self.env["APK"], add_deps)

        if need_deps:
            com.fatal_error(
                "Missing dependencies"
                " (use -r to install them or -R to build+install them):",
                " ".join(need_deps), name=self.msgname)

def undeps(self):
    """
    .. method:: abuild.undeps()

       Uninstall the dependencies that were installed by deps
    """
    args = [self.env["SUDO_APK"]]
    if self.buildroot:
        args += ["--root", self.buildroot]
    args += ["del", self.virtual]

    # apk will return 1 if the virtual doesn't exist, but that's OK
    child.get_retcode(args, env=self.env, retcodes=[1])
