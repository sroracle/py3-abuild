# SPDX-License-Identifier: GPL-2.0-only
# Copyright (c) 2018 Max Rees
# See LICENSE for more information.
"""
Module abuild.child
======================

.. module:: abuild.child
   :synopsis: Convenience functions for logging and executing child processes.
"""
import os         # pipe, set_inheritable, close
import subprocess # Popen, PIPE, run
import logging

import abuild.exception as exc

def get_stdout(*args, **kwargs):
    """
    .. function:: get_stdout(*args, [**kwargs])

       Return the stdout of a child process as a string. Arguments are passed
       to :func:`.run_child`.

       :rtype: str
    """
    return run_child(*args, output="stdout", **kwargs)[1]

def get_stdout_strip(*args, **kwargs):
    """
    .. function:: get_stdout_strip(*args, [**kwargs])

       Similar to :func:`.get_stdout`, but strip leading and trailing
       newlines.

       :rtype: str
    """
    return run_child(*args, output="stdout", **kwargs)[1].strip("\n")

def get_retcode(*args, **kwargs):
    """
    .. function:: get_retcode(*args, [**kwargs])

       Return the return code of a child process as an integer. Arguments are
       passed to :func:`.run_child`.

       :rtype: int
    """
    return run_child(*args, **kwargs)[0]

def _debug(*args, **kwargs):
    logging.getLogger("abuild").debug(*args, **kwargs)

def _die(*args, **kwargs):
    logging.getLogger("abuild").error(*args, **kwargs)
    raise exc.abuildChildError(" ".join(args))

def run_child(*args, output=False, retcodes=[], **kwargs):
    """
    .. function:: run_child(*args, [output=False, retcodes=[], **kwargs])

       Execute a child process. The purpose of this wrapper is so that child
       processes will be logged in debugging output. The arguments are passed
       to :func:`subprocess.run` with some notable exceptions:

       :param output:
          Specifies which output streams should be captured as strings. One of
          ``"both"``, ``"stdout"``, or ``"stderr"``. Streams that are not
          captured are instead sent to the ``logfile`` attribute of the
          ``"abuild"`` logger (if it exists), or otherwise are not touched at
          all.  The default is to not capture any streams as strings.
       :param retcodes:
          A list of integers that denote acceptable return codes for the child
          process. If the child process exits with a nonzero return code and
          the actual return code is not in ``retcodes``, a fatal exception will
          be thrown. A value of ``"all"`` can also be given to accept any
          return code. The default, an empty list, means only a return code of
          zero will be allowed.
       :returns:
          A tuple of the return code, the stdout string (or ``None`` if stdout
          was not captured), and the stderr string (or ``None`` if if stderr was
          not captured).
    """

    # Unfortunately there is a period between when the environ is configured
    # and when the logger is setup where we don't know where we're logging yet.
    # This is because we can't setup the logger until we know if we need colors
    # or not, which requires some commands to be run in common.conf_env(). At
    # that stage we will just output to stdout/stderr as normal.
    logger = logging.getLogger("abuild")
    if hasattr(logger, "logfile"):
        logfile = logger.logfile
    else:
        logfile = None

    if output == "both":
        stdout = subprocess.PIPE
        stderr = subprocess.PIPE
    elif output == "stdout":
        stdout = subprocess.PIPE
        stderr = logfile
    elif output == "stderr":
        stdout = logfile
        stderr = subprocess.PIPE
    else:
        if "stdout" in kwargs:
            stdout = kwargs["stdout"]
            del kwargs["stdout"]
        else:
            stdout = logfile

        if "stderr" in kwargs:
            stderr = kwargs["stderr"]
            del kwargs["stderr"]
        else:
            stderr = logfile

    if isinstance(args[0], list):
        cmd = " ".join(str(arg) for arg in args[0])
    else:
        cmd = args[0]
    _debug("Running command: " + cmd)
    try:
        res = subprocess.run(
            *args, stdout=stdout, stderr=stderr, encoding="utf-8", **kwargs)
    except KeyboardInterrupt:
        _die("Interrupted by user")
    except FileNotFoundError as e:
        _die(str(e))
    except PermissionError as e:
        _die(str(e))

    if res.returncode != 0:
        if retcodes != "all" and res.returncode not in retcodes:
            _die(f"Child exited with status {res.returncode}")

    return (res.returncode, res.stdout, res.stderr)

def _new_child_pipe():
    pipe = os.pipe()
    os.set_inheritable(pipe[0], True)
    os.set_inheritable(pipe[1], True)
    return pipe

def pipeline(*commands, first_stdin=None, last_stdout=None):
    """
    .. function:: pipeline(*commands, [first_stdin=None, last_stdout=None])

       Run a series of commands as a pipeline. The first command will read from
       the file ``first_stdin``, if given. The last command will write to
       ``last_stdout``, if given (otherwise to the usual stdout). At least two
       commands must be given. The commands should be given as a list of
       strings for each command. Creates :class:`subprocess.Popen` objects.

       :param first_stdin:
          Can be a string or a file-like object to feed to the first command in
          the pipeline.
       :param last_stdout:
          A file-like object that will receive the stdout of the last command
          in the pipeline.
       :returns:
          If :data:`subprocess.PIPE` is given as ``last_stdout``, return the
          stdout of the last command in the pipeline. Otherwise return
          ``None``.
    """
    total = len(commands)
    if total < 2:
        raise ValueError("At least two commands must be given")
    if last_stdout == "capture":
        last_stdout = subprocess.PIPE
    processes = []
    pipes = []
    i = 0

    msg = " | ".join(
        [" ".join(str(arg) for arg in command) for command in commands])
    _debug(f"Running pipeline: {msg}")

    for cmd in commands:
        if i == 0:
            pipes.append(_new_child_pipe())

            if isinstance(first_stdin, str):
                stdin = subprocess.PIPE
            else:
                stdin = first_stdin

            if total == 1 and last_stdout:
                stdout = last_stdout
            else:
                stdout = pipes[i][1]

        elif 0 < i < total - 1:
            pipes.append(_new_child_pipe())
            stdin = pipes[i - 1][0]
            stdout = pipes[i][1]

        elif i == total - 1:
            stdin = pipes[i - 1][0]
            stdout = last_stdout

        process = subprocess.Popen(
            cmd, stdin=stdin, stdout=stdout, encoding="utf-8")

        processes.append(process)
        i += 1

    i = 0
    for process in processes:
        if isinstance(first_stdin, str) and i == 0:
            process.communicate(input=first_stdin)
            # XXX Since abuild-tar currently exits with retcode 1, leave this
            # disabled for now.
            #
            #if process.returncode != 0:
            #    _die(f"Child {process.args} exited"
            #         f" with status {process.returncode}")

        else:
            process.wait()
            #if process.wait() != 0:
            #    _die(f"Child {process.args} exited"
            #         f" with status {process.returncode}")

        if i < total - 1:
            os.close(pipes[i][0])
            os.close(pipes[i][1])
        i += 1

    if last_stdout == subprocess.PIPE:
        return processes[-1].stdout.read()

    return None
