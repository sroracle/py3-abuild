# SPDX-License-Identifier: GPL-2.0-only
# Copyright (c) 2018 Max Rees
# See LICENSE for more information.
"""
Module abuild.exception
=======================

.. module:: abuild.exception
   :synopsis: Exception classes raised by :mod:`.abuild`.
"""

class abuildException(Exception):
    def __init__(self, message="", name="abuild"):
        """
        .. class:: abuildException([message="", name="abuild"])

           This is a simple :exc:`Exception` subclass that is raised when a
           fatal error occurs. The :attr:`~.abuildException.name` and
           :attr:`~.abuildException.message` attributes are used to print
           pretty log messages. The :attr:`~.abuildException.name` is often
           given as ``"abuild.{pkgname}"`` (see also
           :meth:`.abuildLogFormatter.format`).
        """
        self.name = name
        self.msg = message

        super().__init__()

    def __str__(self):
        return f"{self.name}: {self.msg}"

class abuildError(abuildException):
    """
    .. class:: abuildError([message="", name="abuild"])

       Raised when an exception occurs that is not necessarily the fault of the
       running build, such as when requisite dependencies are not installed.
       Inherits from :exc:`.abuildException`.
    """
    pass

class abuildFailure(abuildException):
    """
    .. class:: abuildFailure([message="", name="abuild"])

       Raised when an exception occurs that is the fault of the build being
       run, such as files existing in the package that should not be included.
       Inherits from :exc:`.abuildException`.
    """
    pass

class APKBUILDError(abuildException):
    """
    .. class:: APKBUILDError([message="", name="abuild"])

       Raised when an exception occurs in the parsing of an APKBUILD file.
       Inherits from :exc:`.abuildException`.
    """
    pass

class abuildChildError(abuildException):
    """
    .. class:: abuildChildError([message="", name="abuild"])

       Raised when an exception occurs while running a child process - usually
       an unexpected return code. Inherits from :exc:`.abuildException`.
    """
    pass
