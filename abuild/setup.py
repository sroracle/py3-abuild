# SPDX-License-Identifier: GPL-2.0-only
# Copyright (c) 2018 Max Rees
# See LICENSE for more information.
import os     # chdir, access, R_OK
import shutil # rmtree
import shlex  # quote
import mmap   # mmap, ACCESS_READ
import hashlib
from pathlib import Path

import abuild.config as conf
import abuild.common as com
import abuild.child as child
import abuild.exception as exc

def remote_fetch(src, srcdest):
    """
    .. function:: remote_fetch(src, srcdest)

       Fetch a remote source. This calls out to ``abuild-fetch``.

       :param str src:
          The source to pass to abuild-fetch. This can be of the form
          "https://example.com/example.tar.gz" or
          "example-1.0-r0.tar.gz::https://example.com/example.tar.gz".
       :param str srcdest:
          The location where the file should be saved (corresponds to the
          ``-d`` parameter of abuild-fetch; usually is
          ``Abuild.env["SRCDEST"]``).
    """
    child.get_retcode(["abuild-fetch", "-d", srcdest, src])

def clean(self):
    """
    .. method:: Abuild.clean()

       Recursively remove $srcdir and $pkgbasedir
    """
    self.msg("Cleaning temporary build dirs...")
    shutil.rmtree(self.env["srcdir"], ignore_errors=True)
    shutil.rmtree(self.env["pkgbasedir"], ignore_errors=True)

def cleanup(self, error=False):
    """
    .. method:: Abuild.cleanup([error=False])

       Run default $CLEANUP or $ERROR_CLEANUP actions

       If ``-K`` or ``keep_tmp=True`` was given, nothing is done.
    """
    if self.settings["keep_tmp"]:
        return

    needs_cleaning = "ERROR_CLEANUP" if error else "CLEANUP"
    needs_cleaning = self.env[needs_cleaning].split()

    for item in needs_cleaning:
        if item == "pkgdir":
            self.msg("Removing pkgdir")
            shutil.rmtree(self.env["pkgbasedir"], ignore_errors=True)
        elif item == "srcdir":
            self.msg("Removing srcdir")
            shutil.rmtree(self.env["srcdir"], ignore_errors=True)
        elif item == "deps" and self.virtual_installed:
            # No need to uninstall deps: the buildroot will be destroyed
            if "bldroot" in needs_cleaning and self.buildroot:
                continue

            self.msg("Uninstalling dependencies...")
            self.undeps()
        elif item == "bldroot" and self.buildroot:
            self.msg("Removing buildroot...")
            child.run_child(["abuild-rmtemp", self.buildroot])

def fetch(self):
    """
    .. method:: Abuild.fetch()

       Fetch all remote sources and create the symlinks in $srcdir
    """
    self.env["srcdir"].mkdir(exist_ok=True)

    for src in self.source:
        if com.is_remote(src):
            self.msg("Fetching", src, "...")
            remote_fetch(src, self.env["SRCDEST"])
            name = com.filename_from_uri(src)
            target = Path(self.env["SRCDEST"]) / name
            link_name = self.env["srcdir"] / name
        else:
            target = self.env["startdir"] / src
            link_name = self.env["srcdir"] / src

        com.force_symlink(target, link_name)

def verify_checksum(ab, filename, hashtype, old_checksum=None):
    """
    .. function:: verify_checksum(ab, filename, hashtype, old_checksum)

    """
    if hashtype not in hashlib.algorithms_available:
        ab.fatal_error(f"Unsupported hash type {hashtype}")

    filename = Path(filename)
    if not filename.is_file():
        ab.error(f"{filename.name}: cannot find file")
        return False

    if not os.access(filename, os.R_OK):
        ab.error(f"{filename.name}: cannot read file")
        return False

    with open(filename, "r") as f:
        with mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ) as mm:
            new_checksum = hashlib.new(hashtype)
            chunk = mm.read(4096)
            while chunk:
                new_checksum.update(chunk)
                chunk = mm.read(4096)

    new_checksum = new_checksum.hexdigest()
    if old_checksum:
        if new_checksum != old_checksum:
            ab.error(f"{filename.name}: checksum does not match")
            return False

        com.msg2(f"PASS: {filename.name}")

    return new_checksum

def verify(self):
    """
    .. method:: Abuild.verify()

       Verify all checksums in the APKBUILD

       :data:`.CHECKSUMS_READ` determines which checksum properties are
       examined.
    """
    self.fetch()
    os.chdir(self.env["srcdir"])
    res = True

    for algo in conf.CHECKSUMS_READ:
        if com.unset_or_null(self.APKBUILD.meta, algo):
            continue

        hashtype = algo.replace("sums", "")

        self.msg(f"Checking {algo}...")

        csums = {}
        for csum in self.APKBUILD.meta[algo]:
            csum = csum.split(" ", maxsplit=1)
            name = csum[1].strip()
            csums[name] = csum[0].strip()

        if len(csums) != len(self.source):
            self.fail(
                f"number of {algo} ({len(csums)}) !="
                f" number of sources ({len(self.source)})")

        for src in self.source:
            name = com.filename_from_uri(src)

            if not name in csums:
                self.error(f"{name}: missing from {algo}")
                res = False
                continue

            csum = csums[name]

            if not verify_checksum(self, name, hashtype, csum):
                res = False

                if not com.is_remote(src):
                    continue

                else:
                    csum = csum[0:8]
                    path = Path(self.env["SRCDEST"])
                    path_new = path / (name + "." + csum)
                    path = path / name
                    self.error(
                        f"Because the remote file above failed the {algo}"
                        " check it will be renamed.")
                    self.error(f"Renaming {path.name} to {path_new.name}")
                    path.rename(path_new)

    os.chdir(self.env["startdir"])
    if not res:
        raise exc.abuildFailure("checksum failed")

def unpack(self):
    """
    .. method:: Abuild.unpack()

       Unpack any archives listed in $source into $builddir

       .. todo:: Add initdcheck
    """
    self.verify()
    self.env["srcdir"].mkdir(exist_ok=True)

    gunzip = child.get_stdout_strip(
        "command -v pigz || printf gunzip", shell=True)
    if gunzip.endswith("pigz"):
        gunzip += " -d"
    srcdir = str(self.env["srcdir"])
    srcdir_s = shlex.quote(srcdir)

    for src in self.source:
        shell = False
        if com.is_remote(src):
            path = Path(self.env["SRCDEST"]) / com.filename_from_uri(src)
        else:
            path = self.env["startdir"] / src
        path_s = shlex.quote(str(path))

        if path.match("*.tar"):
            args = ["tar", "-C", srcdir, "-xf", str(path)]
        elif path.match("*.tar.gz") or path.match("*.tgz"):
            shell = True
            args = f"{gunzip} -c {path_s} | tar -C {srcdir_s} -f - -x"
        elif path.match("*.tar.bz2"):
            args = ["tar", "-C", srcdir, "-jxf", str(path)]
        elif path.match("*.tar.xz"):
            shell = True
            args = f"unxz --threads=0 -c {path_s} | tar -C {srcdir_s} -f - -x"
        elif path.match("*.zip"):
            args = ["unzip", "-n", "-q", str(path), "-d", srcdir]
        elif path.match("*.tar.lz"):
            args = ["tar", "-C", srcdir, "--lzip", "-xf", str(path)]
        elif path.match("*.tar.lzma"):
            shell = True
            args = f"unlzma -T 0 -c {path_s} | tar -C {srcdir_s} -f - -x"
        else:
            args = []

        if args:
            self.msg("Unpacking", path, "...")
            child.get_retcode(args, shell=shell)

def checksum(self):
    """
    .. method:: Abuild.checksum()

       Update the checksums in the APKBUILD

       All :data:`.CHECKSUMS_READ` properties will be removed, and new ones
       according to `.CHECKSUMS_WRITE` will be written.
       :meth:`.Abuild.fetch` will automatically be called.
    """
    self.fetch()
    os.chdir(self.env["srcdir"])

    for algo in conf.CHECKSUMS_READ:
        if algo in self.APKBUILD.meta:
            self.msg("Removing", algo, "from APKBUILD")
            child.get_retcode([
                "sed", "-i",
                "-e", f"/^{algo}=\".*\"$/d",
                "-e", f"/^{algo}=\"/,/\"$/d",
                "-e", f"/^{algo}='.*'$/d",
                "-e", f"/^{algo}='/,/'$/d",
                str(self.file)])
            del self.APKBUILD.meta[algo]

    srcs = [com.filename_from_uri(src) for src in self.source]

    with open(self.file, "a") as f:
        for algo in conf.CHECKSUMS_WRITE:
            self.msg("Updating", algo, "in APKBUILD...")
            algo = algo.replace("sums", "")

            csums = []
            for src in srcs:
                csum = verify_checksum(self, src, algo)
                if not csum:
                    self.fail(f"Could not generate checksum for {src}")
                csums.append(f"{csum}  {src}")

            self.APKBUILD.meta[algo] = csums
            print("{}=\"{}\"".format(algo + "sums", "\n".join(csums)), file=f)

    os.chdir(self.env["startdir"])

def cleancache(self):
    """
    .. method:: Abuild.cleancache()

       Remove downloaded files from $SRCDEST
    """

    for src in self.source:
        if not com.is_remote(src):
            continue

        filename = com.filename_from_uri(src)
        self.msg(f"Cleaning downloaded {filename}")
        com.safe_rm(self.env["SRCDEST"] / filename)
