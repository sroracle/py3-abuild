Module abuild.config
====================

.. module:: abuild.config
   :synopsis: Default configuration values for the abuild package.

This module contains constants that are used throughout the py3-abuild package.
These typically should only be modified by packagers and not the end user.

.. data:: ABUILD_CONF

   The default path to the system abuild configuration file.  If the
   environment variable :envvar:`ABUILD_CONF` is set, that value will take
   precedence over the default value defined here as ``/etc/abuild.conf``.

.. data:: ABUILD_USERDIR

   The default user directory to look in for the user's personal
   ``abuild.conf``. If the environment variable :envvar:`ABUILD_USERDIR`
   is set, that value will take precedence over the default value defined
   here as ``~/.abuild``.

.. data:: ABUILD_USERCONF

   The default filename for the user's personal ``abuild.conf``. If the
   environment variable :envvar:`ABUILD_USERCONF` is set, that value will
   take precedence over the default value defined here as
   ``$ABUILD_USERDIR/abuild.conf``.

.. data:: REPODEST

    The default directory in which to place built packages. If the
    environment variable :envvar:`REPODEST` is not set, a warning will
    be generated and the default value, defined here as ``~/packages``,
    will be used.

.. data:: NORMAL
.. data:: STRONG

   These are string constants that contain the terminal escape codes for
   reset/normal and bold/strong.

.. data:: RED
.. data:: GREEN
.. data:: YELLOW
.. data:: BLUE

   These are string constants that contain the terminal escape codes for
   their respective colors.
.. data:: COLORS

   This is a mapping of log levels to colors. It is used if the
   :envvar:`USE_COLORS` environment variable is not null and the ``-m``
   command line option was not given, or if the ``-c`` command line option
   was given.

   .. note:: By default, the :class:`.abuild` class does not use colors,
             but the command line program will.

   .. code-block:: python

      COLORS = {
          "CRITICAL": RED,
          "ERROR":    RED,
          "WARNING":  YELLOW,
          "INFO":     GREEN,
          "DEBUG":    BLUE,
      }

.. data:: REQUIRED_PROPERTIES

   This is the list of properties that an APKBUILD *must* have. If the
   APKBUILD does not have one of the properties listed in
   REQUIRED_PROPERTIES, it is a fatal error.

   .. code-block:: python

      REQUIRED_PROPERTIES = (
          "pkgname",
          "pkgver",
          "pkgrel",
          "pkgdesc",
          "url",
          "license",
          "arch",
          # "source", # user/gtk+ has no source?
      )

.. data:: GENERAL_PROPERTIES

   This is the list of APKBUILD properties that are stored internally as a
   string.

   .. code-block:: python

      GENERAL_PROPERTIES = (
          "pkgname",
          "pkgver",
          "pkgrel",
          "pkgdesc",
          "url",
          "license",
          "provider_priority",
          "replaces_priority",
          "builddir",
          "giturl",
          "ldpath",
          "sonameprefix",
      )

.. data:: SPLITTABLE_PROPERTIES

   This is the list of APKBUILD properties that are stored internally as a
   ``list``. The ``*depends*`` properties are stored as a ``set`` instead of
   a ``list`` in order to deduplicate the entries quickly.

   .. code-block:: python

      SPLITTABLE_PROPERTIES = (
          "arch",
          "checkdepends",
          "depends",
          "depends_dev",
          "install",
          "install_if",
          "makedepends",
          "makedepends_build",
          "makedepends_host",
          "pkggroups",
          "pkgusers",
          "provides",
          "options",
          "replaces",
          "source",
          "triggers",
          "somask",
      )

.. data:: CHECKSUMS_READ

   This is the list of APKBUILD ``source`` verification properties that will
   be read from the APKBUILD. In the past, there used to be ``md5sums`` and
   ``sha256sums``, but typically only ``sha512sums`` is used now.

   .. code-block:: python

      CHECKSUMS_READ = (
          "md5sums",
          "sha256sums",
          "sha512sums",
      )

.. data:: CHECKSUMS_WRITE

   This is the list of APKBUILD ``source`` verification properties that will
   be written to the APKBUILD if the ``checksum`` command is used.


   .. code-block:: python

      CHECKSUMS_WRITE = (
          "sha512sums",
      )

.. data:: FUNCS_WITH_DEFAULT

   This is the list of subpackages that have a default split function.

   .. code-block:: python

      FUNCS_WITH_DEFAULT = (
          "dbg",
          "dev",
          "doc",
          "lang",
          "libs",
          "openrc",
      )

.. data:: DEFAULT_NOARCH

   This is a list of globs of packages that should default to
   ``arch=noarch``.  It's typically used to assign the ``arch`` property of
   subpackages when analyzing an APKBUILD.

   .. code-block:: python

      DEFAULT_NOARCH = (
          "*-doc",
          "*-lang",
          "*-lang-*",
          "*-openrc",
      )

.. data:: AVAILABLE_PHASES

   This is the master list of phases that are currently supported. If
   :meth:`.abuild.do_phase` is called with a phase that is not in
   :data:`AVAILABLE_PHASES` or in :data:`.APKBUILD.funcs`, an
   :exc:`.abuildError` is raised.

   .. code-block:: python

      AVAILABLE_PHASES = (
          "all",
          "up2date",
          "deps",
          "undeps",
          "clean",
          "fetch",
          "verify",   # will call fetch
          "unpack",   # will call verify
          "prepare",
          "mkusers",
          "build",
          "check",
          "rootpkg",  # will call package, prepare_all_pkg, create_archives, create_apks
          "package",
          "prepare_all_pkg",
          "create_archives",
          "create_apks",
          "index",
          "cleanup",
      
          "cleancache",
          "checksum", # will call fetch
          "listpkg",
          "cleanpkg",
          "cleanoldpkg",
      
          "rootbld",
          "rootbld_actions",
      )

.. data:: CHECK_REQUIRED

   Whether or not to enforce provision of either a ``check()`` APKBUILD
   function or ``options="!check"``. If ``True``, and the APKBUILD does not
   have either, it is a fatal error. The default is ``False``. A ``check()``
   function is required regardless of this setting if pedantic mode is
   enabled.


.. data:: DEFAULT_PHASES

   The list of phases to perform if :meth:`.abuild.all` is called.

   .. code-block:: python

      DEFAULT_PHASES = (
          "deps",
          "clean",
          "unpack",
          "prepare",
          "mkusers",
          "build",
          "check",
          "rootpkg",
          "index",
          "cleanup",
      )

.. data:: SHELL_PHASES

   The list of phases that need to be performed in the shell.

   .. code-block:: python

      SHELL_PHASES = (
          "build",
          "package",
          *FUNCS_WITH_DEFAULT,
      )

.. data:: ARCH_TO_HOSTSPEC

   A mapping from ``arch`` to ``hostspec`` (also known as a target triplet).
   This is used in the :func:`.arch_to_hostspec` function.

   .. code-block:: python

      ARCH_TO_HOSTSPEC = {
          "aarch64":  "aarch64-foxkit-linux-musl",
          "armel":    "armv5-foxkit-linux-musleabi",
          "armhf":    "armv6-foxkit-linux-muslgnueabihf",
          "armv7":    "armv7-foxkit-linux-musleabihf",
          "i528":     "pentium4-foxkit-linux-musl",
          "mips":     "mips-foxkit-linux-musl",
          "mipsel":   "mipsel-foxkit-linux-musl",
          "mips32":   "mips32-foxkit-linux-musl",
          "mips32el": "mips32el-foxkit-linux-musl",
          "pmmx":     "i586-foxkit-linux-musl",
          "ppc":      "powerpc-foxkit-linux-musl",
          "ppc64":    "powerpc64-foxkit-linux-musl",
          "ppc64le":  "powerpc64le-foxkit-linux-musl",
          "s390x":    "s390x-foxkit-linux-musl",
          "x86":      "i486-foxkit-linux-musl",
          "x86_64":   "x86_64-foxkit-linux-musl",
      }

.. data:: HOSTSPEC_TO_ARCH

   A glob mapping from ``hostspec`` to ``arch``. This is used by the
   :func:`.hostspec_to_arch` function.

   .. code-block:: python

      HOSTSPEC_TO_ARCH = {
          "aarch64*-*-*-*":     "aarch64",
          "arm*-*-*-*eabi":     "armel",
          "armv6*-*-*-*eabihf": "armhf",
          "armv7*-*-*-*eabihf": "armv7",
          "i486-*-*-*":         "x86",
          "i586-*-*-*":         "pmmx",
          "mips-*-*-*":         "mips",
          "mipsel-*-*-*":       "mipsel",
          "mips32-*-*-*":       "mips32",
          "mips32el-*-*-*":     "mips32el",
          "pentium4-*-*-*":     "i528",
          "powerpc-*-*-*":      "ppc",
          "powerpc64-*-*-*":    "ppc64",
          "powerpc64le-*-*-*":  "ppc64le",
          "s390x-*-*-*":        "s390x",
          "x86_64-*-*-*":       "x86_64",
      }

.. data:: HOSTSPEC_TO_LIBC

   A glob mapping from ``hostspec`` to ``libc``. This is used by the
   :func:`.hostspec_to_libc` function.

   .. code-block:: python

      HOSTSPEC_TO_LIBC = {
          "*-*-*-uclibc*": "uclibc",
          "*-*-*-musl*":   "musl",
          "*-*-*-gnu*":    "glibc",
      }

.. data:: SUBPKG_VARS

   A list of properties that may be modified from within a subpackage split
   function. This is used by the :meth:`.abuild.save_env` method. The values
   correspond to the default values: if not the empty string, the default
   value will be inherited from the origin package.

   .. code-block:: python

      SUBPKG_VARS = {
          "depends":           "depends",
          "depends_dev":       "depends_dev",
          "install":           "install",
          "install_if":        [],
          "license":           "license",
          "pkgdesc":           "pkgdesc",
          "provides":          [],
          "provider_priority": "",
          "replaces":          [],
          "replaces_priority": "",
          "triggers":          "triggers",
          "url":               "url",
          "options":           "options",
          "sonameprefix":      "sonameprefix",
          "somask":            "somask",
          "ldpath":            "ldpath",
      }
