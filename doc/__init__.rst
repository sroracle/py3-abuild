Module abuild
====================

.. module:: abuild
   :synopsis: Build packages for systems that use the Alpine Package Keeper.

This module contains the :class:`.Abuild` class, which is used to build
packages according to the specifications in :class:`.APKBUILD` files.

.. class:: Abuild([filename="APKBUILD", env=os.environ])

   Create a new build object.

   :param filename:
      The filename to the APKBUILD that specifies this build. This can
      also be a directory, in which case ``"APKBUILD"`` will be
      appended.
   :type param: str or :class:`pathlib.Path`
   :param mapping settings:
      Settings for this build. See :attr:`.Abuild.settings`.
   :param mapping env:
      The environment for this build. The default is
      :data:`os.environ`.

   :param deps:
      Options for the dependency solver. Can be any value of
      :class:`.DepOpts`, the default being :attr:`.DepOpts.NONE`.
   :param dag:
      Options for the directed acyclic graph builder for the dependency
      solver. Can be any value of :class:`.DAGOpts`, the default being
      :attr:`.DAGOpts.YES`.
   :param bool force:
      Whether to force the build regardless of up2date status.
   :param str loglevel:
      The minimum threshold for log messages. Can be any of "DEBUG",
      "INFO", "WARNING", "ERROR", or "CRITICAL".
   :param bool keep_pkg:
      If ``True``, the ``up2date`` phase will only check for missing
      APKs, not if the APKBUILD or sources are newer.
   :param bool keep_tmp:
      If ``True``, the ``cleanup`` phase will not perform any action.
   :param bool pedantic:
      This option is passed to :class:`.APKBUILD`.
   :param bool mk_untrusted_index:
      When building the APKINDEX in the ``index`` phase, pass the
      ``--allow-untrusted`` option to ``apk``. This is primarily used
      for the test suite so that the example packager private key
      distributed with the source does not need to be installed to
      ``/etc/apk/keys`` in order for the test suite to pass.
   :param str root_nonce:
      Use the specified string as part of the buildroot path instead
      of a randomly generated nonce. The full path will be of the
      form ``"/var/tmp/abuild.{root_nonce}"``.
   :param bool upgrade_root:
      Whether to run update and upgrade the buildroot before
      building the package.
   :param dag_inst:
      An instance of :class:`.Digraph` for use with the dependency
      solver. This parameter is primarily used for recursive
      rebuilding.
   :param dict pkg_db:
      A dictionary mapping APKBUILD filenames to :class:`.Dependency`
      instances. This parameter is primarily used for recursive
      rebuilding.

.. attribute:: Abuild.settings

   This is the dictionary of settings for the build.

.. attribute:: Abuild.env

   The environment variables associated with this build. These
   variables are passed to shell phases
   (:meth:`.Abuild.shell_do_phase`). It is initialized as a copy of
   the ``env`` argument to :class:`.Abuild` which defaults to
   :data:`os.environ`. Example keys include ``startdir``,
   ``srcdir``, ``pkgbasedir`` which are initialized on class
   initialization.

   .. todo:: This maybe should be passed to all children as well?

   .. code-block:: python

              self.env = env.copy()
              self.env["startdir"] = self.file.parent.resolve()
              self.env["srcdir"] = self.env["startdir"] / "src"
              self.env["pkgbasedir"] = self.env["startdir"] / "pkg"
.. attribute:: Abuild.APKBUILD

   This is the :class:`.APKBUILD` object associated with this build.
   It is passed :attr:`.Abuild.env` as its ``env`` argument.
.. attribute:: Abuild.msgname

   The logger name to use for this build. The default value is
   ``abuild.`` plus the main package name. If the
   :envvar:`FAKEROOTKEY` environment variable is not empty in
   :attr:`.Abuild.env`, then a star is appended to the logger name.
.. method:: Abuild.__getattr__(name)

   Syntactic sugar to allow the usage of ``Abuild.name`` instead of
   ``Abuild.APKBUILD.meta["name"]``.

.. method:: Abuild.msg(*args, **kwargs)

.. method:: Abuild.die(*args, **kwargs)

.. method:: Abuild.error(*args, **kwargs)

.. method:: Abuild.warning(*args, **kwargs)

.. method:: Abuild.debug(*args, **kwargs)

   These are convenience methods that will pass :attr:`.Abuild.msgname`
   to the appropriate logging function.

.. method:: Abuild.do_phase(phases, [capture=False])

   Perform the specified phase(s). This function will in turn call
   either the appropriate method of the :class:`.Abuild` class, or call
   :meth:`.Abuild.shell_do_phase`. For most purposes it is recommended
   to call this method to perform a phase instead of any other because
   it performs verification of the phase name.

   :param phase:
       The name of the phase as a string for a single phase, or an
       iterable of phase name strings.
   :type param: iterable or str
   :param capture:
       This is only useful for shell phases and is passed to
       :meth:`.Abuild.shell_do_phase`.

.. method:: Abuild.shell_do_phase(phase, [capture=False])

   Drop into a shell to perform a phase.

   :param str phase:
       The name of the phase to execute. If there was no function
       detected for this phase previously, the "default_*" version of
       the phase will be executed instead.
   :param capture:
       If given as a :class:`dict`, the variables will be stored there.

.. method:: Abuild.save_env(capture, tmp)

   Save the environment variables stored in the file-like object at
   ``tmp`` into the ``capture`` argument.

.. method:: Abuild.all()

   (default) If not up2date, run all of the default phases

   The default phases are determined by :data:`.DEFAULT_PHASES`.

