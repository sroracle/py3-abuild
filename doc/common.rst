Module abuild.common
====================

.. module:: abuild.common
   :synopsis: Small, common functions used by the py3-abuild package.

.. function:: unset_or_null(env, name)

   Returns True if ``name`` is either not in ``env``, or if ``env[name]``
   is falsy.

   :param mapping env: The mapping to check against.
   :param hashable name: The key to check for.
   :rtype: bool

.. function:: set_default(env, name, default)

   If name is either not in ``env``, or ``env[name]`` is falsy, perform
   ``env[name] = default``.

   :param mapping env: The mapping to check against.
   :param hashable name: The key to check for.
   :param default: The default value to assign to ``env[name]``.

.. function:: debug(*args, [name="abuild", **kwargs])

.. function:: msg(*args, [name="abuild", **kwargs])

.. function:: msg2(*args, [name="abuild", **kwargs])

.. function:: warning(*args, [name="abuild", **kwargs])

.. function:: error(*args, [name="abuild", **kwargs])

   These are convenience functions that pass the specified log message to
   the appropriate logger (as determined by ``name``). ``*args`` are
   concatenated into a single string and passed to the appropriate
   :class:`logging.Logger` method along with ``**kwargs``. Note that
   :func:`.msg` corresponds to a log level of ``INFO``.

.. function:: die(*args, [name="abuild", exception=exc.abuildException, **kwargs])

   Same as above, but log a critical error and raise an
   ``exception``.

.. function:: safe_rm(f)

   Similar to ``rm -f``: ignores whether the file exists or not. This is a
   wrapper for :meth:`pathlib.Path.unlink` that discards
   :exc:`FileNotFoundError`.

   :param f: The filename to remove.
   :type f: str or :class:`pathlib.Path`

.. function:: force_symlink(target, link_name)

   Similar to ``ln -sf``: creates a symbolic link named ``link_name`` that
   points to ``target``, overwriting ``link_name`` if it already exists.
   This is a wrapper for :func:`os.symlink` that discards
   :exc:`FileExistsError`.

   :param target: The path that the symlink should point to.
   :type target: str or :class:`pathlib.Path`
   :param link_name: The name to give the symlink.
   :type link_name: str or :class:`pathlib.Path`

.. function:: glob_match(needle, haystack) str:

   Check if ``needle`` matches against any of the globs in ``haystack``.
   This function uses :func:`fnmatch.fnmatchcase` to perform the globbing.

   :param str needle: The string that should be checked.
   :param iterable haystack: The list of glob patterns to match against.
   :rtype: bool

   >>> glob_match("example.tar.gz", ("*.tar.gz", "*.tgz"))
   True
   >>> glob_match("example.tar.bz2", ("*.tar.gz", "*.tgz"))
   False

.. function:: glob_switch(needle, haystack)

   If ``needle`` matches any of the pattern keys in ``haystack``, return
   ``haystack[pattern]``. This function uses :func:`fnmatch.fnmatchcase` to
   perform the globbing.

   .. todo:: Make the no-matches return value configurable.

   :param str needle: The string that should be checked.
   :param mapping haystack:
      The mapping of glob patterns to values to match against.
   :returns:

      The corresponding value for the key ``pattern`` in ``haystack``,
      if ``pattern`` matches ``needle``. If there are no matches, return
      the string ``"unknown"``.

   >>> glob_switch("example.tgz", {"*.tgz": "gunzip", "*.tbz2": "bunzip2"})
   'gunzip'
   >>> glob_switch("example.txz", {"*.tgz": "gunzip", "*.tbz2": "bunzip2"})
   'unknown'

.. function:: arch_to_hostspec(arch)

   This function returns the corresponding ``hostspec`` for ``arch`` as
   defined by :data:`.ARCH_TO_HOSTSPEC`, or the string ``"unknown"`` if
   there is no corresponding ``hostspec``.

.. function:: hostspec_to_arch(hostspec)

   This function returns the corresponding ``arch`` for ``hostspec`` as
   defined by the :data:`.HOSTSPEC_TO_ARCH` glob mapping.

.. function:: hostspec_to_libc(hostspec)

   Return the corresponding ``libc`` string for ``hostspec`` as defined by
   the :data:`.HOSTSPEC_TO_LIBC` glob mapping.

.. function:: is_remote(src)

   Return ``True`` if the source is remote, ``False`` otherwise.

   :rtype: bool

   .. todo:: Make the prefixes that denote a remote source configurable.

   >>> is_remote("example.patch")
   False
   >>> is_remote("https://example.com/example.tar.gz")
   True
   >>> is_remote("example-1.0-r0.tar.gz::https://example.com/example.tar.gz")
   True

.. function:: filename_from_uri(src)

   Return the base filename from a given source. For remote sources,
   this is either the part after the last slash or the component before
   the "::" prefix. For local sources, this is simply the filename.

   :param str src: The ``source`` specification to analyze.
   :rtype: str

   >>> filename_from_uri("example.patch")
   'example.patch'
   >>> filename_from_uri("https://example.com/example.tar.gz")
   'example.tar.gz'
   >>> filename_from_uri("example-1.0-r0.tar.gz::https://example.com/example.tar.gz")
   'example-1.0-r0.tar.gz'

.. function:: atomize(atom)

   Remove version qualifiers from a package atom.

   :param str atom: The package atom to clean.
   :rtype: str

   >>> atomize("pkg>0.0.1")
   'pkg'
   >>> atomize("pkg<=0.0.1")
   'pkg'

.. function:: safe_getgrnam(group)

   This is a wrapper for :func:`grp.getgrnam` that returns ``False``
   instead of raising :exc:`KeyError`.

   :rtype: str or bool

.. function:: safe_getpwnam(user)

   This is a wrapper for :func:`pwd.getpwnam` that returns ``False``
   instead of raising :exc:`KeyError`.

   :rtype: str or bool

.. function:: parse_trigger(trigger)

   Split a ``triggers`` specification into the package name, filename, and
   directories. Raises an exception if the specification has no associated
   directories or filename extension.

   :rtype: tuple

   >>> parse_trigger("subpkg.trigger=/dir1/*:/dir2/*")
   ('subpkg', 'subpkg.trigger', '/dir1/*:/dir2/*')

.. function:: resolve_symlink_to_root(symlink_path, root)

   Read the symbolic link located at ``symlink_path`` and normalize it with
   respect to ``root``. It is assumed that the symlink exists within
   ``root``. Returns the ``symlink_path`` less ``root``, the relative
   target of the symlink, and the resolved target of the symlink with
   respect to ``root``.

   :rtype: tuple

.. function:: parse_install(install)

   Split a ``install`` specification into the package name and filename.
   Raises an exception if the specification has no filename extension.

   :rtype: tuple

   >>> parse_install("subpkg.pre-install")
   ('subpkg', 'subpkg.pre-install')

.. function:: human_readable_bytesize(size)

   Convert the given size in bytes to a human readable size.

   :param int size:
   :rtype: str

   >>> human_readable_bytesize(0)
   '0.0 B'
   >>> human_readable_bytesize(1023)
   '1023.0 B'
   >>> human_readable_bytesize(1024)
   '1.0 KiB'
   >>> human_readable_bytesize(154435553)
   '147.3 MiB'
   >>> human_readable_bytesize(6000000000)
   '5.6 GiB'
   >>> human_readable_bytesize(1.154E18)
   '1025.0 PiB'

.. class:: abuildLogFormatter([fmt=None, use_color=True, show_time=False, **kwargs])

   This is the log formatter used for all messages in the py3-abuild
   package. The ``fmt`` argument should probably be left alone because
   the default already handles ``use_color``, ``show_time``, and
   pretty prints both the log level and the logger name. See
   :meth:`.abuildLogFormatter.format` for more information.

   .. code-block:: python

              if not fmt:
                  if show_time:
                      fmt = "%(magenta)s%(asctime)s "
                  else:
                      fmt = ""
                  fmt += "%(levelcolor)s%(prettylevel)s"
                  fmt += "%(normal)s%(strong)s%(prompt)s%(normal)s%(message)s"
.. method:: abuildLogFormatter.format(record)

   Format a log record. In addition to the usual ``%(message)s`` and
   ``%(asctime)s`` substitutions, this method also substitutes:

   ``%(levelcolor)s``
      The color for the logging level as defined by :data:`.COLORS`.
      This will be the empty string if ``use_colors`` is ``False``.
   ``%(prettylevel)s``
      The logging level plus a colon and a space.  If the logging
      level is ``INFO``, this is the empty string.
   ``%(prompt)s``
      The logger name without the ``abuild.`` prefix (e.g.
      ``pkgname``if the logger name is ``abuild.pkgname``) plus a colon
      and a space. If the prompt ends in an asterisk (denoting presence
      of a fakeroot environment) and ``use_colors`` is ``True``, the
      asterisk will be colored blue.
   ``%(normal)s``, ``%(strong)s``, etc
      The colors as defined in :mod:`.abuild.config`. These will be
      the empty string if ``use_colors`` is ``False``.

.. function:: init_logger(name, [output=sys.stderr, level="INFO", colors=False, time=False])

   Convenience function to initialize a :class:`logging.Logger` object and
   attach a :class:`logging.StreamHandler` to it pointed to ``output``,
   along with a :class:`.abuildLogFormatter` to format the messages.
   ``name`` is used for the logger name and ``level`` for the default
   logging threshold (i.e. only log messages with this log level or higher
   are handled). ``colors`` and ``time`` are passed to the
   :class:`.abuildLogFormatter`.

   :rtype: :class:`logging.Logger`

