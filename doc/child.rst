Module abuild.child
======================

.. module:: abuild.child
   :synopsis: Convenience functions for logging and executing child processes.

.. function:: get_stdout(*args, [**kwargs])

   Return the stdout of a child process as a string. Arguments are passed
   to :func:`.run_child`.

   :rtype: str

.. function:: get_stdout_strip(*args, [**kwargs])

   Similar to :func:`.get_stdout`, but strip leading and trailing
   newlines.

   :rtype: str

.. function:: get_retcode(*args, [**kwargs])

   Return the return code of a child process as an integer. Arguments are
   passed to :func:`.run_child`.

   :rtype: int

.. function:: run_child(*args, [output=False, retcodes=[], **kwargs])

   Execute a child process. The purpose of this wrapper is so that child
   processes will be logged in debugging output. The arguments are passed
   to :func:`subprocess.run` with some notable exceptions:

   :param output:
      Specifies which output streams should be captured as strings. One of
      ``"both"``, ``"stdout"``, or ``"stderr"``. Streams that are not
      captured are instead sent to the ``logfile`` attribute of the
      ``"abuild"`` logger (if it exists), or otherwise are not touched at
      all.  The default is to not capture any streams as strings.
   :param retcodes:
      A list of integers that denote acceptable return codes for the child
      process. If the child process exits with a nonzero return code and
      the actual return code is not in ``retcodes``, a fatal exception will
      be thrown. A value of ``"all"`` can also be given to accept any
      return code. The default, an empty list, means only a return code of
      zero will be allowed.
   :returns:
      A tuple of the return code, the stdout string (or ``None`` if stdout
      was not captured), and the stderr string (or ``None`` if if stderr was
      not captured).

.. function:: pipeline(*commands, [first_stdin=None, last_stdout=None])

   Run a series of commands as a pipeline. The first command will read from
   the file ``first_stdin``, if given. The last command will write to
   ``last_stdout``, if given (otherwise to the usual stdout). At least two
   commands must be given. The commands should be given as a list of
   strings for each command. Creates :class:`subprocess.Popen` objects.

   :param first_stdin:
      Can be a string or a file-like object to feed to the first command in
      the pipeline.
   :param last_stdout:
      A file-like object that will receive the stdout of the last command
      in the pipeline.
   :returns:
      If :data:`subprocess.PIPE` is given as ``last_stdout``, return the
      stdout of the last command in the pipeline. Otherwise return
      ``None``.

