Module abuild.exception
=======================

.. module:: abuild.exception
   :synopsis: Exception classes raised by :mod:`.abuild`.

.. class:: abuildException([message="", name="abuild"])

   This is a simple :exc:`Exception` subclass that is raised when a
   fatal error occurs. The :attr:`~.abuildException.name` and
   :attr:`~.abuildException.message` attributes are used to print
   pretty log messages. The :attr:`~.abuildException.name` is often
   given as ``"abuild.{pkgname}"`` (see also
   :meth:`.abuildLogFormatter.format`).

.. class:: abuildError([message="", name="abuild"])

   Raised when an exception occurs that is not necessarily the fault of the
   running build, such as when requisite dependencies are not installed.
   Inherits from :exc:`.abuildException`.

.. class:: abuildFailure([message="", name="abuild"])

   Raised when an exception occurs that is the fault of the build being
   run, such as files existing in the package that should not be included.
   Inherits from :exc:`.abuildException`.

.. class:: APKBUILDError([message="", name="abuild"])

   Raised when an exception occurs in the parsing of an APKBUILD file.
   Inherits from :exc:`.abuildException`.

.. class:: abuildChildError([message="", name="abuild"])

   Raised when an exception occurs while running a child process - usually
   an unexpected return code. Inherits from :exc:`.abuildException`.

