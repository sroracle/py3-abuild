Module abuild.file
======================

.. module:: abuild.file
   :synopsis: Classes for representation of APKBUILD and PKGINFO files.

.. class:: PKGINFO(filename)

   A subclass of :class:`collections.OrderedDict` that allows multiple
   keys of the same name by initializing nonexistent keys to an empty
   list, then appending values to that list. The ``filename`` parameter
   is used by :meth:`.PKGINFO.writeout`.

   >>> p = PKGINFO("pkg/.control-pkgname/.PKGINFO")
   >>> p["depend"] = "dep1"
   >>> p["depend"] = "dep2"
   >>> p["depend"] = "dep2"
   >>> p["depend"] = ["dep3", "dep4"]
   >>> p["depend"] = ["dep1", "dep5"]
   >>> p["depend"]
   ['dep1', 'dep2', 'dep3', 'dep4', 'dep5']

.. method:: PKGINFO.writeout([mode="w"])

   Open the associated file with the given ``mode`` and write the
   ``{key} = {value}`` pairs to the file. An entry is written for each
   value string in the ``PKGINFO[key]`` list.

.. class:: APKBUILD(filename, [contents=None, env=environ, pedantic=False])

   Create a new APKBUILD representation object.

   :param filename:
      The path to the APKBUILD, or a fake path if ``contents`` is
      given.
   :param str contents:
      The contents of the APKBUILD as a string. If this is non-empty, it
      will be used instead of reading the file at ``filename``.
   :param env:
      The environment to pass to the shell parser when expanding the
      APKBUILD. It is important that there is a ``srcdir`` key so that
      custom ``builddir`` settings are expanded properly. If there is
      no ``srcdir`` key, it will be set to be a subdirectory of the
      directory containing ``filename`` named "src".
   :param bool pedantic:
      Whether to enforce certain constraints on read APKBUILDs.
      Currently, this will force the provision of a ``check()``
      function in all APKBUILDs regardless of the
      :data:`.CHECK_REQUIRED` global configuration, and will also move
      ``-dbg`` subpackages to the top of the ``subpackages`` list.

.. attribute:: APKBUILD.meta

   This dictionary contains most of the properties of the APKBUILD.
   It is typically not used directly because of
   :meth:`.APKBUILD.__getattr__`.
.. attribute:: APKBUILD.expected_funcs

   This is a set of shell function names that, based on the contents
   of ``subpackages``, are expected to be present in the APKBUILD
   file. By default it also contains ``package()``.
.. attribute:: APKBUILD.funcs

   This is the set of shell functions that are actually present in
   the APKBUILD file.
.. todo::

   Should :meth:`.APKBUILD.__init__` call :meth:`.APKBUILD.expand`,
   :meth:`.APKBUILD.calc_subpkg_funcs`, and :meth:`.APKBUILD.parse`?
.. method:: APKBUILD.__getattr__(name)

   Syntactic sugar to allow the usage of ``APKBUILD.name`` instead of
   ``APKBUILD.meta["name"]``.

.. method:: APKBUILD.warning(*args, **kwargs)

.. method:: APKBUILD.die(*args, **kwargs)

   Convenience methods for the logging functions from
   :mod:`.abuild.common` that set the logger name to ``abuild.`` plus
   the main package name.

.. method:: APKBUILD._expand()

   Internal function to execute the shell process that will return the
   raw expanded APKBUILD file.

.. method:: APKBUILD.expand()

   Inspect the APKBUILD and fill the :attr:`.APKBUILD.meta` dictionary
   with the APKBUILD's properties. This function performs a basic
   sanity check by making sure that each of
   :data:`.REQUIRED_PROPERTIES` is present in the APKBUILD. It will
   then set the rest of :data:`.GENERAL_PROPERTIES` and
   :data:`.SPLITTABLE_PROPERTIES` to null values.

.. method:: APKBUILD.dump_info()

   This is a convenience function that will pretty print
   :attr:`.APKBUILD.meta` to directly to ``stdout``.

.. method:: APKBUILD.calc_subpkg_funcs()

   This method populates the :attr:`.APKBUILD.expected_funcs` attribute
   with a function name for each of the subpackages listed in
   `subpackages`. If the subpackage was written in the APKBUILD in
   either the form ``pkgname-sub:splitfunc`` or
   ``pkgname-sub:splitfunc:arch`` (but not ``pkgname-sub::arch``), the
   name will be ``splitfunc``.  Otherwise, it will be ``sub`` (i.e the
   segment after the final dash).

.. method:: APKBUILD.parse()

   Open the APKBUILD file directly and parse for ``Contributor`` lines,
   ``Maintainer`` lines, or shell functions. If
   :data:`.CHECK_REQUIRED` is True, ``!check`` is not in ``options``,
   and there is no ``check()`` function in the APKBUILD, an exception
   will be thrown. Additionally, if any of the functions in
   :attr:`.APKBUILD.expected_funcs` are missing, and those functions
   have no default (as defined by :data:`.FUNCS_WITH_DEFAULT`), an
   exception will be thrown.

