py3-abuild documentation
========================

.. toctree::
   :maxdepth: 1

   __init__
   child
   common
   config
   exception
   file

* :ref:`genindex`
* :ref:`search`
