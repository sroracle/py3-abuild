PYTHON        = python3
DESTDIR       = /
PREFIX        = usr

SPHINXOPTS    =
SPHINXBUILD   = sphinx-build-3

CHECKTARGETS  = abuild/file.py abuild/common.py
DOCTARGETS    = doc/__init__.rst \
                doc/child.rst \
                doc/common.rst \
                doc/config.rst \
                doc/exception.rst \
                doc/file.rst
EXTRADIST     = LICENSE* \
                TODO.rst \
                Makefile \
                tests \
                helpers

# Build
all: helpers/printenv module
module:
	$(PYTHON) setup.py build
helpers/printenv: helpers/printenv.c

# Tests
check: helpers/printenv
	$(PYTHON) -m doctest -v $(CHECKTARGETS)
	$(PYTHON) tests/run_tests.py

# Install
install: module helpers/printenv
	$(PYTHON) setup.py install --prefix="/$(PREFIX)" --root="$(DESTDIR)"

clean:
	rm -rf helpers/printenv
	rm -rf build pkg *.egg-info
	rm -rf tests/*/*.log tests/*/src tests/*/pkg tests/apks
	rm -rf doc/_build
	rm -rf dist *.tar.gz py3-abuild-*

# Maintainer: documentation
doc: $(DOCTARGETS)
	@$(SPHINXBUILD) -M html doc doc/_build $(SPHINXOPTS) $(O)
doc/%.rst: abuild/%.py
	doc/mkdocs.py < "$<" > "$@"

# Maintainer: distribution
dist: clean
	@[ -z "$$(git status --porcelain)" ] || \
		{ echo "Uncommitted changes"; exit 1; }
	$(PYTHON) setup.py sdist -k
	rm -rf py3-abuild-*/*.egg-info py3-abuild-*/setup.cfg py3-abuild-*/PKG-INFO
	rm -rf py3-abuild-*/abuild/helpers
	ln -sf ../helpers py3-abuild-*/abuild/
	cp -a $(EXTRADIST) py3-abuild-*/
	bsdtar -zcf dist/py3-abuild-*.tar.gz --uname root --gname root py3-abuild-*

.PHONY: module install clean dist
