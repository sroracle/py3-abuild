/*  Copyright (c) 2018 Max Rees
 *  Distributed under GPL-2.0-only
 *  See LICENSE for more information.
 */
#include <stdlib.h>

int main(void) {
    asm("call abort;");
}
