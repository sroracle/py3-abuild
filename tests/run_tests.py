#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-2.0-only
# Copyright (c) 2018 Max Rees
# See LICENSE for more information.
# pylint: disable=invalid-name,redefined-outer-name
import glob    # iglob
import sys     # argv, stdout
import pathlib # Path
import os      # chdir, environ
import re      # compile
import shutil  # rmtree
import tarfile # open, ReadError, CompressionError

APK_CHECKS = (
    "exists",
    "mode",
    "link",
    "uname",
    "gname",
)

testdir = pathlib.Path(os.getcwd())
if (testdir / "tests").is_dir():
    testdir /= "tests"
    os.chdir(testdir)
parent = testdir.parent.absolute()
os.environ["ABUILD_BUILDROOT"] = ""
os.environ["PATH"] = f"{parent}:{os.environ['PATH']}"
os.environ["PYTHONPATH"] = str(parent)
os.environ["ABUILD_USERDIR"] = str(testdir)
os.environ["ABUILD_USERCONF"] = str(testdir / "abuild.conf")
os.environ["PACKAGER"] = "pbuild automated testing <pbuild-test@example.com>"
os.environ["PACKAGER_PRIVKEY"] = str(testdir / "privkey")
sys.path.append(os.environ["PYTHONPATH"])

# pylint: disable=wrong-import-position
import abuild.common as com
import abuild.child as child

def msg(*args, name="abuild-tester", **kwargs):
    com.msg(*args, name=name, **kwargs)

def msg2(*args, name="abuild-tester", **kwargs):
    com.msg2(*args, name=name, **kwargs)

def error(*args, name="abuild-tester", **kwargs):
    com.error(*args, name=name, **kwargs)

def die(*args, name="abuild-tester", **kwargs):
    com.error(*args, name=name, **kwargs)
    sys.exit(1)

def save_output(stdout, stderr):
    pathlib.Path("stdout.log").write_text(stdout)
    pathlib.Path("stderr.log").write_text(stderr)

def check_patterns(output, patterns):
    if not patterns:
        return

    for line in output.splitlines():
        for pattern in patterns:
            if pattern.match(line):
                patterns.remove(pattern)

def fail_patterns(name, logname, patterns):
    if not patterns:
        return False

    error(f"Failed {name} output matches:", name=logname)
    for pattern in patterns:
        msg2(repr(pattern.pattern), name=logname)

    return True

def fail_apks(apk_globs, tests):
    failed = False

    for (test_type, test_arg, filename) in tests:
        apks_globbed = str(repodest / "tests" / "*" / apk_globs)
        apks = glob.iglob(apks_globbed)
        if not apks:
            error(f"'{apks_globbed}': no matches")
            failed = True
            continue

        for apk in apks:
            try:
                tar = tarfile.open(apk, mode="r:gz")
            except (tarfile.ReadError, tarfile.CompressionError) as e:
                error(str(e))
                failed = True
                continue

            try:
                entry = tar.getmember(filename)
            except KeyError:
                error(f"{apk}: Could not find {filename}")
                tar.close()
                failed = True
                continue

            if test_type == "exists":
                pass

            if test_type == "mode" and entry.mode != int(test_arg, 8):
                error(
                    f"{apk}: {filename}:"
                    f" mode is {oct(entry.mode)}, not {test_arg}")
                failed = True

            if test_type == "link":
                ftype = entry.type
                if not ftype in (tarfile.LNKTYPE, tarfile.SYMTYPE):
                    error(
                        f"{apk}: {filename}:"
                        f" is {ftype}, not symbolic link")
                    failed = True
                elif entry.linkname != test_arg:
                    error(
                        f"{apk}: {filename}:"
                        f" target is {entry.linkname}, not {test_arg}")
                    failed = True

            if test_type == "uname" and entry.uname != test_arg:
                error(
                    f"{apk}: {filename}:"
                    f" owner is {entry.uname}, not {test_arg}")
                failed = True

            if test_type == "gname" and entry.gname != test_arg:
                error(
                    f"{apk}: {filename}:"
                    f" group is {entry.gname}, not {test_arg}")
                failed = True

            tar.close()

    return failed

if not (testdir / "run_tests.py").is_file():
    die("Could not find tests!")

com.init_logger("abuild-tester", colors=sys.stdout.isatty())

repodest = testdir / "apks"
shutil.rmtree(repodest, ignore_errors=True)
repodest.mkdir(mode=0o755, parents=True)
os.environ["APORTSDIR"] = str(testdir.resolve())
os.environ["REPODEST"] = str(repodest.resolve())

if len(sys.argv) == 1:
    tests = glob.iglob(str(testdir / "*" / "APKBUILD"))
else:
    tests = sys.argv[1:]

fails = 0
for test in tests:
    path = pathlib.Path(test).parent
    name = path.name
    logname = "abuild-tester." + name
    args = [parent / "pbuild", "-m", "--mk-untrusted-index"]
    stdout_re = []
    stderr_re = []
    pkginfo_re = {}
    apk_check = {}
    retcodes = [0]

    com.msg("Testing...", name=logname)

    with open(test) as f:
        for line in f.readlines():
            line = line.strip()

            if line.startswith("#:args: "):
                args += line.replace("#:args: ", "").split()

            elif line.startswith("#:stdout: "):
                line = line.replace("#:stdout: ", "")
                stdout_re.append(re.compile(line))

            elif line.startswith("#:stderr: "):
                line = line.replace("#:stderr: ", "")
                stderr_re.append(re.compile(line))

            elif line.startswith("#:retcodes: "):
                line = line.replace("#:retcodes: ", "").split()
                retcodes = [int(i) for i in line]

            elif line.startswith("#:PKGINFO:"):
                line = line.replace("#:PKGINFO:", "")
                pkg, pattern = line.split(":", maxsplit=1)
                if pkg not in pkginfo_re:
                    pkginfo_re[pkg] = []
                pkginfo_re[pkg].append(re.compile(pattern[1:]))

            elif line.startswith("#:apk:"):
                line = line.replace("#:apk:", "")
                apks, fileish = line.split(":", maxsplit=1)
                fileish = fileish.strip().split(" ", maxsplit=3)
                # Only check for existence
                if len(fileish) == 1:
                    fileish = ["exists", None, fileish[0]]
                # <test type> <test arg> <filename>
                elif len(fileish) != 3:
                    die("Invalid #:apk: directive")
                if fileish[0] not in APK_CHECKS:
                    die(f"Invalid #:apk: test type '{fileish[0]}'")
                if apks not in apk_check:
                    apk_check[apks] = []
                apk_check[apks].append(fileish)

    if pkginfo_re:
        args.append("-K")

    os.chdir(path)
    ret, stdout, stderr = child.run_child(args, output="both", retcodes="all")
    stdout = stdout.strip()
    stderr = stderr.strip()

    if ret not in retcodes:
        save_output(stdout, stderr)
        error(f"Child exited with status {ret}", name=logname)
        fails += 1
        continue

    if not stderr:
        save_output(stdout, stderr)
        error("No output generated!", name=logname)
        fails += 1
        continue

    check_patterns(stdout, stdout_re)
    check_patterns(stderr, stderr_re)

    failed = fail_patterns("stdout", logname, stdout_re)
    failed = fail_patterns("stderr", logname, stderr_re) or failed

    for pkg in pkginfo_re:
        check_patterns(
            open(f"pkg/.control.{pkg}/.PKGINFO").read(), pkginfo_re[pkg])
        failed = fail_patterns(
            f"{pkg} PKGINFO", logname, pkginfo_re[pkg]) or failed

    for apk_globs in apk_check:
        failed = fail_apks(apk_globs, apk_check[apk_globs]) or failed

    if failed:
        save_output(stdout, stderr)
        fails += 1

    os.chdir(testdir)

sys.exit(fails)
