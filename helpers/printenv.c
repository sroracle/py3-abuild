/* SPDX-License-Identifier: GPL-2.0-only
 * Copyright (c) 2018 Max Rees
 * See LICENSE for more information.
 */
#include <stdio.h>
extern char **environ;

int main(void) {
    char **envp;

    for (envp = environ; envp && *envp; envp++) {
        fputs(*envp, stdout);
        fputc(0, stdout);
    }

    return 0;
}
