#!/bin/sh -ea
# SPDX-License-Identifier: GPL-2.0-only
# Copyright (c) 2018 Max Rees
# See LICENSE for more information.
while [ -n "$1" ]; do
	case "$1" in
	/*) . "$1";;
	-) . /dev/stdin;;
	*) . "./$1";;
	esac
	shift
done
"${0%/*}/printenv"
