#!/bin/sh
# SPDX-License-Identifier: GPL-2.0-only
# Copyright (c) 2018 Max Rees
# See LICENSE for more information.
for APKBUILD in $(find "$APORTSDIR" -name APKBUILD); do
	pkgname=
	subpackages=
	replaces=
	provides=

	. "$APKBUILD"
	# If there isn't even a package name, let's move along
	[ -z "$pkgname" ] && continue

	printf "%s\t%s\t\n" "$pkgname" "$APKBUILD"
	for name in $subpackages $replaces $provides; do
		name="${name%%:*}"
		name="${name%%[<>=~]*}"
		[ -z "$name" ] && continue

		printf "%s\t%s\t%s\n" "$pkgname" "$APKBUILD" "$name"
	done
done
